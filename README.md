## Falcon.SugarApi    

### Nuget包地址   
在VS中增加程序包源地址：`http://106.14.65.137/api/packages/Falcon/nuget/index.json`然后就可以在Nuget包管理中引入`Falcon.SugarApi `包。


### 引入包说明   
引入nuget包可能默认不会包含Falcon.SugarApi.xml文件，但是没有这个文件swagger会少很多说明，也可能会报错。    
将一下代码包含在.csproj项目文件中可以复制包中的xml文件到输出目录和发布目录。   
```xml
<Target Name="CopyReferenceFiles" BeforeTargets="Build">
	<ItemGroup>
		<XmlReferenceFiles Condition="Exists('$(OutputPath)%(Filename).dll')" Include="%(Reference.RelativeDir)%(Reference.Filename).xml" />
	</ItemGroup>
	<Message Text="Copying reference files to $(OutputPath)" Importance="High" />
	<Copy SourceFiles="@(XmlReferenceFiles)" DestinationFolder="$(OutputPath)" Condition="Exists('%(RootDir)%(Directory)%(Filename)%(Extension)')" />
</Target>
<Target Name="CopyReferenceFilesToPublish" BeforeTargets="PrepareForPublish">
	<ItemGroup>
		<XmlReferenceFiles Condition="Exists('$(OutputPath)%(Filename).dll')" Include="%(Reference.RelativeDir)%(Reference.Filename).xml" />
	</ItemGroup>
	<Message Text="Copying reference files to $(OutputPath)" Importance="High" />
	<Copy SourceFiles="@(XmlReferenceFiles)" DestinationFolder="$(PublishDir)" Condition="Exists('%(RootDir)%(Directory)%(Filename)%(Extension)')" />
</Target>
```

### WebApi扩展模块  [进入](/Falcon/Falcon.SugarApi/src/branch/master/Falcon.SugarApi/ApiDefinistions)   
>  `ApiControllerBase`是所有webapi基类，实现数据库、log、异常等基础功能。   
>  `ApiBaseOptionController<>`带有基本操作方法的webapi基类，除了`ApiControllerBase`功能外还提供基础增删改查功能。   

### Swagger扩展模块 [进入](/Falcon/Falcon.SugarApi/src/branch/master/Falcon.SugarApi/Swagger)     
>  `waggerGenOptions.AddXmlEnumEnable`方法可以为枚举类型增加注释。   

### SugarDbContext扩展模块 [进入](/Falcon/Falcon.SugarApi/src/branch/master/Falcon.SugarApi/DatabaseDefinitions)   
>  通过`IServiceCollection.AddSugarApiDbContext` 注册`SugarDbContext`对象，该对象通过`SugarConnectionConfig`配置数据库链接。   
>  `DbSet<T>`数据库表定义，支持表数据查询。    
>  `SugarDbTables<T>`数据库表集合基类，可以继承该类并在其中定义`DbSet<T>`属性，这些属性会自动实例化。  
>  `SugarTableBase`标准表基类，继承该类可提供数据表基础功能，比如ID，创建和修改时间等。  

### 后台任务BackTask模块  [进入](/Falcon/Falcon.SugarApi/src/branch/master/Falcon.SugarApi/BackTask)   
>  继承`BackgroundLongTask`可以实现一个长期位于后台执行的对象，该类通过`IServiceCollection.AddHostedService<>'进行注册。
>  `RunTimespan`属性设置执行的时间间隔的秒数。    
>  重写`RunAsync`方法实现一个异步任务。   
>  另外可以重写`OnStart`后台任务开始，`OnStop`后台任务结束，`OnCompleted`一次执行完成和`OnException`执行中引发未处理的异常。    

### 数据缓冲模块  [进入](/Falcon/Falcon.SugarApi/src/branch/master/Falcon.SugarApi/Cache)   
>  通过`IServiceCollection.AddSugarRedisCache` 或 `IServiceCollection.AddSugarMemoryCache`方法注册缓冲器。    
>  通过注入 `ISugarCache`同步接口 或 `ISugarCacheAsync`异步接口获取组件。   
>  通过接口的`Get`和`Set`方法获取和保存缓冲值。   

### XML序列化扩展模块  [进入](/Falcon/Falcon.SugarApi/src/branch/master/Falcon.SugarApi/XmlSerialize)   
>  `IServiceCollection.AddXmlSerializeFactory`方法注册xml序列化工厂`XmlSerializeFactory`，可以通过该工厂实例化一个XML序列化器。   

### JSON序列化扩展模块  [进入](/Falcon/Falcon.SugarApi/src/branch/master/Falcon.SugarApi/JsonSerialize)   
>  `IServiceCollection.AddJsonSerializeFactory`方法注册Json序列化工厂`JsonSerializeFactory`，可以通过该工厂实例化一个Json序列化器。   
>  `ArrayStringJsonConverterAttribute`特性标记字符串是个数组字符串，当进行序列化时候会被序列化为数组。  

### 字符串扩展方法    
>  `IsNullOrEmpty()`和`IsNotNullOrEmpty()`方法返回字符串是否为空。   
>  `SplitStr()`默认实现按照`',', '，', ';', '；', '.', '。'`对字符串进行分割。  

### Object扩展方法   
>  `CloneTo`和`CloneFrom`实现将对象属性赋值到目标对象同名属性中，此为浅表复制。   

### IEnumerable扩展方法   
>  `ToDataTable`和`ToDataTable<>`分别实现将枚举类型转换为DataTable的方法，区别是一个针对Object枚举一个针对具体类型枚举，尽量使用泛型版本，更加高效。   


﻿using Falcon.SugarApi.DatabaseDefinitions;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SqlSugar;
using System.Xml.Linq;
using System.Linq;

namespace Falcon.SugarApi.Test
{
    /// <summary>
    /// 执行存储过程测试
    /// </summary>
    [TestClass]
    public class UseStoredProcedureTest
    {
        /// <summary>
        /// 执行Oracle存储过程
        /// </summary>
        [TestMethod]
        public void RunOracle() {
            using var db = OracleSugarDb;
            Assert.IsNotNull(db);

            Assert.Fail("下面测试需要链接数据并且满足测试条件后注释此条后继续");

            var pn = "GetEmployee";
            //var orgid = "50e3d44d-9ca2-4fbd-9d5d-d32339b1b113";
            //var ps = db.Ado.GetParameters(new { v_orgaid = orgid });
            //var plist = ps.ToList();
            //plist.Add(db.OracleRefCursor());
            //ps = plist.ToArray();

            var org = new SugarParameter(":v_orgaid", "50e3d44d-9ca2-4fbd-9d5d-d32339b1b113");
            //var data = new SugarParameter(":v_data", null, true) { IsRefCursor = true, DbType = System.Data.DbType.String };
            //var data1 = new SugarParameter(":v_data", null, true) { IsRefCursor = true };
            //var data = new SugarParameter(":v_data", "");
            //data.IsRefCursor = true;
            //data.Direction = System.Data.ParameterDirection.Output;
            var rtb = db.Ado.UseStoredProcedure().GetDataTable(pn, org, db.OracleRefCursor());

            //var rtb = db.GetAdo().GetDataTable(pn,org,data);
            Assert.IsNotNull(rtb);
            rtb = db.UseStoredProcedure().GetDataTable(pn, db.GetParameters(new { v_orgaid = "50e3d44d-9ca2-4fbd-9d5d-d32339b1b113" }));

            var aaa = db.GetParameters(new { v_orgaid = "50e3d44d-9ca2-4fbd-9d5d-d32339b1b113" }).Concat(new SugarParameter[] { db.OracleRefCursor() });
            rtb = db.UseStoredProcedure().GetDataTable(pn, aaa);
            Assert.IsNotNull(rtb);
        }

        /// <summary>
        /// Oracle数据库
        /// </summary>
        private SugarDbContext OracleSugarDb {
            get {
                var configurationBuilder = new ConfigurationBuilder();
                configurationBuilder.AddJsonFile("AppSettings.json");
                var config = configurationBuilder.Build();
                var sc = config.GetSection("Database");
                var dbconfig = new SugarConnectionConfig {
                    ConnectionString = sc.GetValue<string>("oracle"),
                    DbType = SqlSugar.DbType.Oracle,
                    Log = sc.GetValue<bool>("log"),
                    IsAutoCloseConnection = true,
                    InitKeyType = InitKeyType.Attribute,
                };
                return new SugarDbContext(dbconfig, new TestLog<SugarDbContext>());
            }
        }
    }
}

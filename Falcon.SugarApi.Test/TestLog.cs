﻿using System;
using Microsoft.Extensions.Logging;

namespace Falcon.SugarApi.Test
{
    /// <summary>
    /// 测试用Logger实例
    /// </summary>
    public class TestLog<T> : ILogger,ILogger<T>
    {
        public IDisposable BeginScope<TState>(TState state) {
            throw new NotImplementedException();
        }

        public bool IsEnabled(LogLevel logLevel) => true;

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter) {
            Console.WriteLine(state?.ToString());
        }
    }
}
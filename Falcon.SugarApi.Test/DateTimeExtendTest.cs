﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Falcon.SugarApi.Test
{
    /// <summary>
    /// 日期时间扩展方法测试
    /// </summary>
    [TestClass]
    public class DateTimeExtendTest
    {
        /// <summary>
        /// 测试过程
        /// </summary>
        [TestMethod]
        public void TestFunction() {
            var dt = DateTime.Now;
            Console.WriteLine($"当前时间：{dt:yyyy-MM-dd}");
            DateTime r;
            int y = dt.Year, m = dt.Month, d = dt.Day;

            r = dt.GetFirstDayOfTheWeek();
            Console.WriteLine($"本周第一天(默认礼拜一)：{r:yyyy-MM-dd}");
            Assert.IsTrue(r.DayOfWeek == DayOfWeek.Monday, "默认星期一为第一天");
            Assert.IsTrue(r.Year == y);
            Assert.IsTrue(r.Month == m);
            Assert.IsTrue(r.Hour == 0);
            Assert.IsTrue(r.Minute == 0);
            Assert.IsTrue(r.Second == 0);

            r = dt.GetFirstDayOfTheWeek(DayOfWeek.Sunday);
            Console.WriteLine($"本周第一天(礼拜天)：{r:yyyy-MM-dd}");
            Assert.IsTrue(r.DayOfWeek == DayOfWeek.Sunday, "默认星期天为第一天");
            Assert.IsTrue(r.Year == y);
            Assert.IsTrue(r.Month == m);

            r = new DateTime(2022, 1, 1);
            Assert.IsTrue(r.GetWeekOfTheYear() == 1);
            r = new DateTime(2022, 1, 5);
            Assert.IsTrue(r.GetWeekOfTheYear() == 2);
            Assert.IsTrue(r.Hour == 0);
            Assert.IsTrue(r.Minute == 0);
            Assert.IsTrue(r.Second == 0);

            r = dt.GetFirstDayOfTheYear();
            Console.WriteLine($"当年的第一天{r:yyyy-MM-dd}");
            Assert.IsTrue(r.Year == dt.Year);
            Assert.IsTrue(r.Month == 1);
            Assert.IsTrue(r.Day == 1);
            Assert.IsTrue(r.Hour == 0);
            Assert.IsTrue(r.Minute == 0);
            Assert.IsTrue(r.Second == 0);

            r = dt.GetDay(new DateOption {
                DateRangeType = DateRangeType.Monthly, DateType = DateType.FirstDay, RetentionTime = true,
            });
            Console.WriteLine($"当月的第一天保持时间{r:yyyy-MM-dd HH:mm:ss}");
            Assert.IsTrue(r.Year == dt.Year);
            Assert.IsTrue(r.Month == dt.Month);
            Assert.IsTrue(r.Day == 1);
            Assert.IsTrue(r.Hour == dt.Hour);
            Assert.IsTrue(r.Minute == dt.Minute);
            Assert.IsTrue(r.Second == dt.Second);

            r = dt.GetFirstDayOfTheMonth();
            Console.WriteLine($"当月的第一天{r:yyyy-MM-dd HH:mm:ss}");
            Assert.IsTrue(r.Year == dt.Year);
            Assert.IsTrue(r.Month == dt.Month);
            Assert.IsTrue(r.Day == 1);
            Assert.IsTrue(r.Hour == 0);
            Assert.IsTrue(r.Minute == 0);
            Assert.IsTrue(r.Second == 0);

            r = dt.GetDay(new DateOption { DateType = DateType.LastDay });
            Console.WriteLine($"当月的最后一天{r:yyyy-MM-dd HH:mm:ss}");
            Assert.IsTrue(r.Year == dt.Year);
            Assert.IsTrue(r.Month == dt.Month);
            Assert.IsTrue(r.Day == 29 || r.Day == 30 || r.Day == 31 );
            Assert.IsTrue(r.Hour == 0);
            Assert.IsTrue(r.Minute == 0);
            Assert.IsTrue(r.Second == 0);

        }

        /// <summary>
        /// 时间格式转换测试
        /// </summary>
        [TestMethod]
        public void DateToFormatTest() {
            var d = new DateTime(2022, 1, 4, 2, 40, 25);
            var s = d.ToyyyyMMdd();
            Assert.IsTrue(s == "20220104");
            s = d.ToyyyyMMddHHmmss();
            Assert.IsTrue(s == "20220104024025");
        }
    }
}

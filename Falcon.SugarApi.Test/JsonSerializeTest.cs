﻿using Falcon.SugarApi.JsonSerialize;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.Json;

namespace Falcon.SugarApi.Test
{
    /// <summary>
    /// Json序列化测试
    /// </summary>
    [TestClass]
    public class JsonSerializeTest {

        /// <summary>
        /// 注入测试
        /// </summary>
        [TestMethod]
        public void JsonSerializeDITest() {
            var service = new ServiceCollection();
            service.AddSingleton<IJsonSerialize, Falcon.SugarApi.JsonSerialize.JsonSerialize>();
            service.AddSingleton<JsonSerializerOptions>(new JsonSerializerOptions());
            var provider = service.BuildServiceProvider();
            var _ = provider.GetService<IJsonSerialize>();

            service = new ServiceCollection();
            service.AddSingleton<IJsonSerialize, Falcon.SugarApi.JsonSerialize.JsonSerialize>();
            provider = service.BuildServiceProvider();
            _ = provider.GetService<IJsonSerialize>();
        }
    }
}

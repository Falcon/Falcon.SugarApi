﻿using Falcon.SugarApi.ModelValidation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Falcon.SugarApi.Test
{
    /// <summary>
    /// Json序列化测试
    /// </summary>
    [TestClass]
    public class ModelValidationTest
    {
        [TestMethod]
        public void ValidateTest() {
            TestModel model;
            List<ValidationResult> results;
            bool r;

            model = new TestModel {
                Age = "30",
                Name = "Tom",
            };
            r = model.TryModelValidation(out results);
            Assert.IsTrue(r);
            Assert.IsTrue(results != null);
            Assert.IsTrue(results.Count == 0);

            model = new TestModel {
                Age = "60",
            };
            r = model.TryModelValidation(out results);
            Assert.IsFalse(r);
            Assert.IsTrue(results.Count == 2);

            model = new TestModel {
                MaxLength = "111111111111111111",
                Name = "Tom",
                Age = "30",
            };
            r = model.TryModelValidation(out results);
            Assert.IsFalse(r);
            Assert.IsTrue(results.Count == 1);

            model = new TestModel {
                Name = "abc",
                Age = "20",
                IsIntProp = "abc",
                MaxLength = "1",
            };
            r = model.TryModelValidation(out results);
            Assert.IsFalse(r);
            Assert.IsTrue(results.Count == 1);
            Assert.IsTrue(results.First().MemberNames.First() == "IsIntProp");
        }
        /// <summary>
        /// 测试用模型
        /// </summary>
        public class TestModel
        {
            [Required]
            public string Name { get; set; }

            [Range(10, 50)]
            public string Age { get; set; }

            [MaxLength(3)]
            public string MaxLength { get; set; }

            [IsInt]
            public string IsIntProp { get; set; } = "123";
        }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Falcon.SugarApi.ModelValidation
{
    /// <summary>
    /// 扩展对象验证方法
    /// </summary>
    public static class ObjectValidateExtend
    {
        /// <summary>
        /// 尝试执行对象验证，返回验证结果
        /// </summary>
        /// <param name="model">要验证的模型</param>
        /// <param name="results">验证结果</param>
        /// <returns>通过验证True，否则False</returns>
        public static bool TryModelValidation(this object model, out List<ValidationResult> results) {
            var context = new ValidationContext(model);
            results = new List<ValidationResult>();
            return Validator.TryValidateObject(model, context, results, true);
        }
    }
}

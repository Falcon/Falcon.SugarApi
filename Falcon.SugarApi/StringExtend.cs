﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

namespace Falcon.SugarApi
{
    /// <summary>
    /// 字符串扩展
    /// </summary>
    public static class StringExtend
    {
        /// <summary>
        /// 字符串是否为空或null
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns>空或null为True，否则False</returns>
        public static bool IsNullOrEmpty(this string? str) => str==null||string.IsNullOrEmpty(str);
        /// <summary>
        /// 字符串是否不为空或null
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns>与IsNullOrEmpty相反</returns>
        public static bool IsNotNullOrEmpty(this string? str) => !str.IsNullOrEmpty();

        /// <summary>
        /// 当字符串IsNullOrEmpty为True的时候返回默认值，否则返回本身
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="defaultVal">默认值</param>
        /// <returns>字符串本身或默认值</returns>
        public static string? ToDefault(this string? str,[NotNull] string defaultVal) => str.IsNullOrEmpty() ? defaultVal : str;

        /// <summary>
        /// 使用全角半角的逗号句号和分号分割字符串。
        /// </summary>
        /// <param name="str">要分割的字符串</param>
        /// <param name="splitChars"></param>
        /// <returns>字符串数组。当str为null时返回空数组</returns>
        public static string[] SplitStr(this string? str,params char[] splitChars)
            => str.IsNullOrEmpty() ? Array.Empty<string>() : str.Split(splitChars.Length==0 ? new char[] { ',','，',';','；','.','。' } : splitChars,StringSplitOptions.RemoveEmptyEntries|StringSplitOptions.TrimEntries);

        /// <summary>
        /// 将字符串按格式转换为时间格式
        /// </summary>
        /// <param name="str">时间字符串</param>
        /// <param name="dateTimeFormat">时间格式</param>
        /// <param name="culture">区域特性.默认CultureInfo.InvariantCulture</param>
        /// <returns>时间</returns>
        public static DateTime ToDateTime(this string str,string dateTimeFormat = "",CultureInfo? culture = null) {
            culture??=CultureInfo.InvariantCulture;
            if(dateTimeFormat.IsNullOrEmpty()) {
                return DateTime.Parse(str);
            }
            return DateTime.ParseExact(str,dateTimeFormat,culture);
        }
        /// <summary>
        /// 尝试将特定格式字符串转换为DateTime类型
        /// </summary>
        /// <param name="str">时间字符串</param>
        /// <param name="dateTimeFormat">时间格式</param>
        /// <param name="dt">转换后的时间</param>
        /// <param name="culture">区域特性.默认CultureInfo.InvariantCulture</param>
        /// <returns>成功True，失败False</returns>
        public static bool TryToDateTime(this string str,string dateTimeFormat,out DateTime dt,CultureInfo? culture = null) {
            try {
                dt=str.ToDateTime(dateTimeFormat,culture);
                return true;
            }
            catch(Exception) {
                dt=default;
                return false;
            }
        }
        /// <summary>
        /// 尝试将特定格式字符串转换为DateTime类型
        /// </summary>
        /// <param name="str">时间字符串</param>
        /// <param name="dt">转换后的时间</param>
        /// <param name="dateTimeFormat">时间格式</param>
        /// <param name="culture">区域特性.默认CultureInfo.InvariantCulture</param>
        /// <returns>成功True，失败False</returns>
        public static bool TryToDateTime(this string str,out DateTime dt,string dateTimeFormat = "",CultureInfo? culture = null)
           => str.TryToDateTime(dateTimeFormat,out dt);
        /// <summary>
        /// 返回字符串是否为指定格式的日期时间格式
        /// </summary>
        /// <param name="str">给定字符串</param>
        /// <param name="dateFormat">日期时间格式</param>
        /// <returns>是True，否False</returns>
        /// <exception cref="ArgumentNullException">参数为空</exception>
        public static bool IsDateFormat(this string str,string dateFormat) {
            if(str.IsNullOrEmpty()) {
                throw new ArgumentNullException(nameof(str));
            }
            if(dateFormat.IsNullOrEmpty()) {
                throw new ArgumentNullException(nameof(dateFormat));
            }
            return str.TryToDateTime(dateFormat,out var _);
        }

        /// <summary>
        /// 获取子字符串
        /// <para>如果start大于字符串长度或者length等于0则返回空字符串。</para>
        /// <para>如果length小于0则返回start前面长度的字符串。</para>
        /// <para>如果字符串不满足需要的长度要求则返回实际长度字符串。</para>
        /// <para>如果start小于0，则返回从0开始的length+start长度字符串。</para>
        /// </summary>
        /// <param name="str">要检索的字符串</param>
        /// <param name="start">开始位置</param>
        /// <param name="length">获取长度</param>
        /// <returns>子字符串</returns>
        /// <exception cref="Exception">获取长度不满足要求</exception>
        public static string SubstringEx(this string str,int start,int length) {
            if(start>str.Length||length==0) {
                return "";
            }
            if(length<0) {
                start=start+length;
                length=-length;
            }
            if(start<0) {
                length+=start;
                start=0;
            }
            if(start+length>str.Length) {
                return str.Substring(start);
            }
            return str.Substring(start,length);
        }

        /// <summary>
        /// 按照固定长度分割字符串
        /// </summary>
        /// <param name="str">要分割的字符串</param>
        /// <param name="length">分割长度</param>
        /// <returns>分割后的字符串枚举</returns>
        public static IEnumerable<string> Split(this string str,int length) {
            if(length<=0) {
                throw new Exception("分割长度必须大于0");
            }
            if(str.IsNullOrEmpty()) {
                throw new ArgumentNullException(nameof(str));
            }
            int i = 0;
            while(str.Length>i) {
                yield return str.SubstringEx(i,length);
                i+=length;
            }
        }
    }
}
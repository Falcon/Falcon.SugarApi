﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

namespace Falcon.SugarApi
{
    /// <summary>
    /// 类型相关扩展
    /// </summary>
    public static class TypeExtend
    {

        /// <summary>
        /// 尝试获取Attribute
        /// </summary>
        /// <typeparam name="T">Attribute类型</typeparam>
        /// <param name="info">属性</param>
        /// <param name="p">定义的特性</param>
        /// <returns>定义返回True，否则False</returns>
        public static bool TryGetAttribute<T>([NotNull] this PropertyInfo info, out T p) where T : Attribute {
            p = info.GetCustomAttribute<T>();
            return p != null;
        }

        /// <summary>
        /// 尝试获取Attribute
        /// </summary>
        /// <typeparam name="T">Attribute类型</typeparam>
        /// <param name="info">属性</param>
        /// <param name="p">定义的特性</param>
        /// <returns>定义返回True，否则False</returns>
        public static bool TryGetAttribute<T>([NotNull] this Type info, out T p) where T : Attribute {
            p = info.GetCustomAttribute<T>();
            return p != null;
        }
    }
}

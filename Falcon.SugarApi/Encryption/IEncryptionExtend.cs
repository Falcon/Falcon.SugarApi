﻿using Falcon.SugarApi.JsonSerialize;
using System;

namespace Falcon.SugarApi.Encryption
{
    /// <summary>
    /// IEncryption接口扩展
    /// </summary>
    public static class IEncryptionExtend
    {
        /// <summary>
        /// 对象序列化接口
        /// </summary>
        public static IJsonSerialize JsonSerialize { get; set; } = new JsonSerializeFactory().CreateJsonSerialize();

        /// <summary>
        /// 加密对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="encryption">加密接口</param>
        /// <param name="obj">要加密的对象</param>
        /// <param name="key">加密用的key</param>
        /// <returns>加密后结果</returns>
        /// <exception cref="ArgumentNullException">参数为null</exception>
        public static string Encrypt<T>(this IEncryption encryption, string key, T obj) {
            obj.ThrowNullExceptionWhenNull();
            key.ThrowNullExceptionWhenNull();
            return encryption.Encrypt(key, JsonSerialize.Serialize(obj));
        }

        /// <summary>
        /// 解密对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="encryption">加密接口</param>
        /// <param name="key">加密用的key</param>
        /// <param name="strObj">对象密文</param>
        /// <returns>解密后的对象</returns>
        /// <exception cref="ArgumentNullException">参数为null</exception>
        public static T? Decrypt<T>(this IEncryption encryption, string key, string strObj)
            where T : class {
            key.ThrowNullExceptionWhenNull();
            strObj.ThrowNullExceptionWhenNull();
            return JsonSerialize.Deserialize<T>(encryption.Decrypt(key, strObj));
        }
    }
}
﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Falcon.SugarApi.Encryption
{
    /// <summary>
    /// DES对称加密
    /// </summary>
    public class DESProvider : IEncryption, IDESEncryption
    {
        /// <summary>
        /// 配置文件
        /// </summary>
        public DESConfig Config { get; set; }

        /// <summary>
        /// 通过提供配置创建DES提供程序
        /// </summary>
        /// <param name="config"></param>
        public DESProvider(DESConfig config) {
            Config = config;
        }

        /// <summary>
        /// 创建加解密提供程序
        /// </summary>
        /// <param name="key">加密用的key</param>
        /// <returns>加解密提供器</returns>
        private DESCryptoServiceProvider createProvider(string key) {
            if (key.Length < 8) {
                throw new Exception("加密用的key长度为8位");
            }
            byte[] bKey = this.Config.Encoding.GetBytes(key.Substring(0, 8));
            return new DESCryptoServiceProvider() {
                Padding = Config.Padding,
                Mode = Config.Mode,
                IV=Config.IV,
                Key = bKey,
            };
        }

        /// <inheritdoc/>
        public string GenerateKey() {
            var p = new DESCryptoServiceProvider() {
                Padding = Config.Padding,
                Mode = Config.Mode,
                IV = Config.IV,
            };
            p.GenerateKey();
            return Convert.ToBase64String(p.Key);
        }

        /// <inheritdoc/>
        public string Decrypt(string key, string str) {
            var bStr = Convert.FromBase64String(str);
            var desc = createProvider(key);
            using (var mStream = new MemoryStream()) {
                using (var cStream = new CryptoStream(mStream, desc.CreateDecryptor(), CryptoStreamMode.Write)) {
                    cStream.Write(bStr, 0, bStr.Length);
                    cStream.FlushFinalBlock();
                    var bs = mStream.ToArray();
                    return this.Config.Encoding.GetString(bs);
                }
            }
        }

        /// <inheritdoc/>
        public string Encrypt(string key, string str) {
            byte[] bStr = this.Config.Encoding.GetBytes(str);
            var desc = createProvider(key);
            using (var mStream = new MemoryStream()) {
                using (var cStream = new CryptoStream(mStream, desc.CreateEncryptor(), CryptoStreamMode.Write)) {
                    cStream.Write(bStr, 0, bStr.Length);
                    cStream.FlushFinalBlock();
                    var bs = mStream.ToArray();
                    return Convert.ToBase64String(bs);
                }
            }
        }
    }
}

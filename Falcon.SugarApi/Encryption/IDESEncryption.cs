﻿namespace Falcon.SugarApi.Encryption
{
    /// <summary>
    /// DES加密算法接口
    /// </summary>
    public interface IDESEncryption : IEncryption { }
}
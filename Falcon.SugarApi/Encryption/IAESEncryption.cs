﻿namespace Falcon.SugarApi.Encryption
{
    /// <summary>
    /// AES加密算法接口
    /// </summary>
    public interface IAESEncryption : IEncryption { }
}
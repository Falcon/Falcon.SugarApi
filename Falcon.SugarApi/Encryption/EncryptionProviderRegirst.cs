﻿using Microsoft.Extensions.DependencyInjection;
using System;


namespace Falcon.SugarApi.Encryption
{
    /// <summary>
    /// 安全组件注册
    /// </summary>
    public static class EncryptionProviderRegirst
    {
        /// <summary>
        /// 注册DES加密提供程序，通过IEncryption或IDESEncryption获取注册程序。
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <param name="config">配置方法</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddDesProvider(this IServiceCollection services, Action<DESConfig> config) {
            var defConfig = new DESConfig();
            config(defConfig);
            services.AddSingleton<IEncryption>(new DESProvider(defConfig));
            services.AddSingleton<IDESEncryption>(new DESProvider(defConfig));
            return services;
        }

        /// <summary>
        /// 使用默认配置注册DES加密提供程序，通过IEncryption或IDESEncryption获取注册程序。
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddDesProvider(this IServiceCollection services) {
            var defConfig = new DESConfig();
            services.AddSingleton<IEncryption>(new DESProvider(defConfig));
            services.AddSingleton<IDESEncryption>(new DESProvider(defConfig));
            return services;
        }

        /// <summary>
        /// 注册AES加密提供程序，通过IEncryption或IAESEncryption获取注册程序。
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <param name="config">配置方法</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddAesProvider(this IServiceCollection services, Action<AESConfig> config) {
            var defConfig = new AESConfig();
            config(defConfig);
            services.AddSingleton<IEncryption>(new AESProvider(defConfig));
            services.AddSingleton<IAESEncryption>(new AESProvider(defConfig));
            return services;
        }

        /// <summary>
        /// 使用默认配置注册AES加密提供程序，通过IEncryption或IAESEncryption获取注册程序。
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddAesProvider(this IServiceCollection services) {
            var defConfig = new AESConfig();
            services.AddSingleton<IEncryption>(new AESProvider(defConfig));
            services.AddSingleton<IAESEncryption>(new AESProvider(defConfig));
            return services;
        }

    }
}

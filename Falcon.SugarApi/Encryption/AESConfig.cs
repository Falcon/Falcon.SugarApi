﻿using Microsoft.Extensions.Options;

namespace Falcon.SugarApi.Encryption
{
    /// <summary>
    /// AES加密算法配置
    /// </summary>
    public class AESConfig : IOptions<AESConfig>
    {
        /// <inheritdoc />
        public AESConfig Value => this;

        private int keyLength = 32;

        /// <summary>
        /// 秘钥长度。最大32
        /// </summary>
        public int KeyLength { get => keyLength; set => keyLength = value > 32 ? 32 : value; }

        /// <summary>
        /// 秘钥字符表
        /// </summary>
        public string KeyChars { get; set; } = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789,.!/*\";

    }
}

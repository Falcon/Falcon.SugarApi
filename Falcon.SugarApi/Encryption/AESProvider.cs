﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Falcon.SugarApi.Encryption
{
    /// <summary>
    /// AES对称加密算法
    /// </summary>
    public class AESProvider : IEncryption, IAESEncryption
    {
        /// <summary>
        /// 通过提供配置构造AES加密实例
        /// </summary>
        /// <param name="config">配置文件</param>
        public AESProvider(AESConfig config) {
            Config = config;
        }

        /// <summary>
        /// 加密配置
        /// </summary>
        public AESConfig Config { get; }

        /// <inheritdoc/>
        public string Decrypt(string key, string str) {
            var fullCipher = Convert.FromBase64String(str);
            var iv = new byte[16];
            var cipher = new byte[fullCipher.Length - iv.Length];
            Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, fullCipher.Length - iv.Length);
            var decryptKey = GetDecryptKey(key);
            using (var aesAlg = Aes.Create()) {
                using (var decryptor = aesAlg.CreateDecryptor(decryptKey, iv)) {
                    string result;
                    using (var msDecrypt = new MemoryStream(cipher)) {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)) {
                            using (var srDecrypt = new StreamReader(csDecrypt)) {
                                result = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                    return result;
                }
            }
        }

        /// <inheritdoc/>
        public string Encrypt(string key, string str) {
            var encryptKey = GetDecryptKey(key);
            using (var aesAlg = Aes.Create()) {
                var iv = aesAlg.IV;
                using (var encryptor = aesAlg.CreateEncryptor(encryptKey, iv)) {
                    using (var msEncrypt = new MemoryStream()) {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        using (var swEncrypt = new StreamWriter(csEncrypt)) {
                            swEncrypt.Write(str);
                        }
                        var decryptedContent = msEncrypt.ToArray();
                        var result = new byte[iv.Length + decryptedContent.Length];
                        Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                        Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);
                        return Convert.ToBase64String(result);
                    }
                }
            }
        }

        /// <inheritdoc/>
        public string GenerateKey() {
            var chars = this.Config.KeyChars;
            var r = new Random();
            var len = this.Config.KeyLength;
            var sb = new StringBuilder(len);
            for (int i = 0; i < len; i++) {
                sb.Append(chars[r.Next(0, chars.Length)]);
            }
            return sb.ToString();
        }

        /// <summary>
        /// 格式化加密key长度
        /// </summary>
        /// <param name="key">加密用的key</param>
        /// <returns>加密key字节数组</returns>
        protected byte[] GetDecryptKey(string key) {
            var buf = new byte[this.Config.KeyLength];
            var ek = Encoding.UTF8.GetBytes(key);
            var len = this.Config.KeyLength > ek.Length ? ek.Length : buf.Length;
            Buffer.BlockCopy(ek, 0, buf, 0, len);
            return buf;
        }

    }
}

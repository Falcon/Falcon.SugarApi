﻿namespace Falcon.SugarApi.Encryption
{
    /// <summary>
    /// 基础加密解密接口
    /// </summary>
    public interface IEncryption
    {
        /// <summary>
        /// 通过key解密字符串
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="str">密文</param>
        /// <returns>明文</returns>
        string Decrypt(string key, string str);
        /// <summary>
        /// 通过key加密字符串
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="str">明文</param>
        /// <returns>密文</returns>
        string Encrypt(string key, string str);
        /// <summary>
        /// 生成加密key
        /// </summary>
        /// <returns>key</returns>
        string GenerateKey();
    }
}
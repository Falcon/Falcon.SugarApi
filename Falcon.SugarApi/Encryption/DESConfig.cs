﻿using Microsoft.Extensions.Options;
using System.Security.Cryptography;
using System.Text;

namespace Falcon.SugarApi.Encryption
{
    /// <summary>
    /// DES加密配置
    /// </summary>
    public class DESConfig:IOptions<DESConfig>
    {
        /// <summary>
        /// 加解密编码方式
        /// </summary>
        public Encoding Encoding { get; set; } = Encoding.UTF8;
        /// <summary>
        /// 填充方式
        /// </summary>
        public PaddingMode Padding { get; set; } = PaddingMode.PKCS7;
        /// <summary>
        /// 加密模式
        /// </summary>
        public CipherMode Mode { get; set; } = CipherMode.CBC;
        /// <summary>
        /// 加密用的IV
        /// </summary>
        public byte[] IV { get; set; } = { 123, 221, 221, 111, 4, 6, 7, 22 };

        /// <inheritdoc/>
        public DESConfig Value => this;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Falcon.SugarApi.JsonSerialize
{
    /// <summary>
    /// 序列化字符串属性到数组
    /// </summary>
    public class ArrayJsonConverter : JsonConverter<string>
    {
        /// <summary>
        /// 字符串分隔符数组
        /// </summary>
        public char[]? SplitChars { get; set; } = null;

        /// <inheritdoc/>
        public override string? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
            if (reader.TokenType != JsonTokenType.StartArray) {
                throw new Exception($"传入的数据类型错误！应该为数组，现在为{reader.TokenType.ToString()}");
            }
            var list = new List<string>();
            while (reader.Read()) {
                if (reader.TokenType == JsonTokenType.StartArray) {
                    continue;
                }
                if (reader.TokenType == JsonTokenType.EndArray) {
                    break;
                }
                var str = reader.GetString().ToDefault("")!;
                list.Add(str);
            }
            return string.Join(',', list.ToArray());
        }
        /// <inheritdoc/>
        public override void Write(Utf8JsonWriter writer, string value, JsonSerializerOptions options) {
            var array = this.SplitChars == null ? value.SplitStr() : value.SplitStr(this.SplitChars);
            writer.WriteStartArray();
            for (int i = 0; i < array.Length; i++) {
                writer.WriteStringValue(array[i]);
            }
            writer.WriteEndArray();
        }
    }

    /// <summary>
    /// 标记属性，表示这个一个数组，序列化时将会被序列化为一个以特殊字符分割的字符串数组
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ArrayStringJsonConverterAttribute : JsonConverterAttribute
    {
        /// <summary>
        /// 标记属性，表示这个一个数组，序列化时将会被序列化为一个以特殊字符分割的字符串数组
        /// </summary>
        public ArrayStringJsonConverterAttribute() : base(typeof(ArrayJsonConverter)) { }
    }

}

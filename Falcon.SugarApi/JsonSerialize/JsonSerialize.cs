﻿using System;
using System.Text.Json;

namespace Falcon.SugarApi.JsonSerialize
{
    /// <summary>
    /// 完成JSON序列化
    /// </summary>
    public class JsonSerialize : IJsonSerialize
    {
        /// <summary>
        /// 默认选项
        /// </summary>
        public JsonSerializerOptions Options { get; set; }

        /// <summary>
        /// 构造序列化器
        /// </summary>
        public JsonSerialize() : this(new JsonSerializerOptions()) { }

        /// <summary>
        /// 通过选项构造默认序列化器
        /// </summary>
        /// <param name="options"></param>
        public JsonSerialize(JsonSerializerOptions options) {
            Options = options;
        }

        /// <summary>
        /// 反序列化json字符串
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="str">json字符串</param>
        /// <returns>json对象</returns>
        public T? Deserialize<T>(string str) where T : class
            => JsonSerializer.Deserialize<T>(str, Options);

        /// <summary>
        /// 序列化json对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="obj">json对象</param>
        /// <returns>json字符串</returns>
        public string Serialize<T>(T obj)
            => JsonSerializer.Serialize(obj,Options);

        /// <summary>
        /// 反序列化json字符串
        /// </summary>
        /// <param name="str">json字符串</param>
        /// <param name="returnType">返回类型</param>
        /// <returns>json对象</returns>
        public object? Deserialize(string str, Type returnType) 
            => JsonSerializer.Deserialize(str, returnType,Options);

        /// <summary>
        /// 序列化json对象
        /// </summary>
        /// <param name="obj">json对象</param>
        /// <param name="inputType">输入类型</param>
        /// <returns>json字符串</returns>
        public string Serialize(object obj, Type inputType) 
            => JsonSerializer.Serialize(obj, inputType, Options);

    }
}

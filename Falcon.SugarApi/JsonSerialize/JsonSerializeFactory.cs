﻿using System.Text.Json;

namespace Falcon.SugarApi.JsonSerialize
{
    /// <summary>
    /// Json序列化器工厂
    /// </summary>
    public class JsonSerializeFactory
    {
        /// <summary>
        /// 使用默认设置创建Json序列化器
        /// </summary>
        /// <returns>序列化器</returns>
        public IJsonSerialize CreateJsonSerialize() {
            return new JsonSerialize();
        }

        /// <summary>
        /// 使用默认设置创建Json序列化器
        /// </summary>
        /// <returns>序列化器</returns>
        public IJsonSerialize CreateJsonSerialize(JsonSerializerOptions options) {
            return new JsonSerialize(options);
        }

    }
}

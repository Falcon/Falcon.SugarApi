﻿using Microsoft.Extensions.Options;
using System;
using System.Globalization;

namespace Falcon.SugarApi
{
    /// <summary>
    /// 时间类型扩展
    /// </summary>
    public static class DateTimeExtend
    {
        #region 获取某个时间
        /// <summary>
        /// 获取日期在某个时间段内的某一天
        /// </summary>
        /// <param name="date">日期</param>
        /// <param name="option">查询选项</param>
        /// <returns>符合条件日期</returns>
        public static DateTime GetDay(this DateTime date, DateOption option) {
            date = option.RetentionTime ? date : new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            var drt = option.DateRangeType;
            var dt = option.DateType;
            var firstDayOfWeek = option.DayOfWeek;
            return
                drt == DateRangeType.Weekly && dt == DateType.FirstDay ? date.AddDays(firstDayOfWeek - date.DayOfWeek) :
                drt == DateRangeType.Weekly && dt == DateType.LastDay ? date.AddDays(firstDayOfWeek - date.DayOfWeek + 7) :
                drt == DateRangeType.Monthly && dt == DateType.FirstDay ? date.AddDays(1 - date.Day) :
                drt == DateRangeType.Monthly && dt == DateType.LastDay ? date.AddDays(1 - date.Day).AddMonths(1).AddDays(-1) :
                drt == DateRangeType.Yearly && dt == DateType.FirstDay ? new DateTime(date.Year, 1, 1) :
                drt == DateRangeType.Yearly && dt == DateType.LastDay ? new DateTime(date.Year, 1, 1).AddYears(1).AddDays(-1) :

                throw new NotSupportedException("不支持这个日期查询");
        }


        /// <summary>
        /// 返回本周的第一天的日期。默认星期一为第一天
        /// </summary>
        /// <param name="date">本周的一天</param>
        /// <param name="firstDayOfWeek">默认第一天是星期几</param>
        /// <returns>本周第一天</returns>
        public static DateTime GetFirstDayOfTheWeek(this DateTime date, DayOfWeek firstDayOfWeek = DayOfWeek.Monday) {
            return date.GetDay(new DateOption { DateRangeType = DateRangeType.Weekly, DateType = DateType.FirstDay, DayOfWeek = firstDayOfWeek, });
        }

        /// <summary>
        /// 获取一个月中的第一天
        /// </summary>
        /// <param name="date">日期</param>
        /// <returns>日期</returns>
        public static DateTime GetFirstDayOfTheMonth(this DateTime date) {
            return date.GetDay(new DateOption { DateRangeType = DateRangeType.Monthly, DateType = DateType.FirstDay });
        }

        /// <summary>
        /// 获取一年中的第一天
        /// </summary>
        /// <param name="date">日期</param>
        /// <returns>日期</returns>
        public static DateTime GetFirstDayOfTheYear(this DateTime date) {
            return date.GetDay(new DateOption { DateRangeType = DateRangeType.Yearly, DateType = DateType.FirstDay });
        }

        /// <summary>
        /// 获取日期为一年中的第几周
        /// </summary>
        /// <param name="date">日期</param>
        /// <param name="firstDayOfWeek">第一天是星期几</param>
        /// <returns>第几周</returns>
        public static int GetWeekOfTheYear(this DateTime date, DayOfWeek firstDayOfWeek = DayOfWeek.Monday) {
            return date.GetDefaultCalendar().GetWeekOfYear(date, CalendarWeekRule.FirstDay, firstDayOfWeek);
        }

        /// <summary>
        /// 获取默认的当前线程区域设置的日历
        /// </summary>
        /// <param name="dateTime">无关紧要，可以是任何时间</param>
        /// <returns></returns>
        public static Calendar GetDefaultCalendar(this DateTime dateTime) {
            return CultureInfo.CurrentCulture.Calendar;
        }

        #endregion

        #region 获取某个时间格式
        /// <summary>
        /// 获取时间的yyyyMMdd表示形式
        /// </summary>
        /// <param name="date">时间</param>
        /// <returns>yyyyMMdd格式时间</returns>
        public static string ToyyyyMMdd(this DateTime date) => date.ToString("yyyyMMdd");

        /// <summary>
        /// 获取时间的yyyyMMddHHmmss表示形式
        /// </summary>
        /// <param name="date">时间</param>
        /// <returns>yyyyMMdd格式时间</returns>
        public static string ToyyyyMMddHHmmss(this DateTime date) => date.ToString("yyyyMMddHHmmss");

        #endregion
    }

    /// <summary>
    /// 日期操作选项
    /// </summary>
    public class DateOption : IOptions<DateOption>
    {
        /// <summary>
        /// 日期类型,默认第一天
        /// </summary>
        public DateType DateType { get; set; } = DateType.FirstDay;
        /// <summary>
        /// 日期范围类型，默认每月
        /// </summary>
        public DateRangeType DateRangeType { get; set; } = DateRangeType.Monthly;
        /// <summary>
        /// 哪一天是一个星期的第一天，默认星期一
        /// </summary>
        public DayOfWeek DayOfWeek { get; set; } = DayOfWeek.Monday;
        /// <summary>
        /// 保持时间不变，默认false
        /// </summary>
        public bool RetentionTime { get; set; } = false;

        /// <summary>
        /// 默认每个月第一天
        /// </summary>
        public DateOption Value => this;
    }
    /// <summary>
    /// 日期类型
    /// </summary>
    public enum DateType
    {
        /// <summary>
        /// 第一天
        /// </summary>
        FirstDay,
        /// <summary>
        /// 最后一天
        /// </summary>
        LastDay,
    }
    /// <summary>
    /// 日期范围类型
    /// </summary>
    public enum DateRangeType
    {
        /// <summary>
        /// 每周
        /// </summary>
        Weekly,
        /// <summary>
        /// 每月
        /// </summary>
        Monthly,
        /// <summary>
        /// 每年
        /// </summary>
        Yearly,
    }
}

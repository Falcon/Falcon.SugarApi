﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Falcon.SugarApi
{
    /// <summary>
    /// 枚举扩展
    /// </summary>
    public static class IEnumerableExtend
    {
        /// <summary>
        /// 枚举转换为DataTable
        /// </summary>
        /// <param name="source">数据枚举</param>
        /// <returns>转换后Datatable</returns>
        /// <exception cref="ArgumentNullException">参数为Null</exception>
        public static DataTable ToDataTable(this IEnumerable<object> source) {
            _ = source ?? throw new ArgumentNullException(nameof(source));
            var dt = new DataTable();
            if(source.Count() == 0) {
                return dt;
            }
            for(int i = 0;i < source.Count();i++) {
                var item = source.ToArray()[i];
                foreach(PropertyInfo pro in item.GetType().GetProperties()) {
                    if(!dt.Columns.Contains(pro.Name)) {
                        dt.Columns.Add(new DataColumn {
                            ColumnName = pro.Name,
                            DataType = pro.PropertyType,
                        });
                    }
                }
            }
            foreach(var i in source) {
                var row = dt.NewRow();
                foreach(var p in i.GetType().GetProperties()) {
                    if(p.CanRead) {
                        var val = p.GetValue(i);
                        try {
                            row[p.Name] = val;
                        } catch(Exception ex) {
                            throw new Exception($"值设置失败！{p.Name}:{val}",ex);
                        }
                    }
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        /// <summary>
        /// 枚举转换为DataTable
        /// </summary>
        /// <typeparam name="T">枚举的元素类型</typeparam>
        /// <param name="source">原数据</param>
        /// <returns>转换后Datatable</returns>
        /// <exception cref="ArgumentNullException">参数为Null</exception>
        public static DataTable ToDataTable<T>(this IEnumerable<T> source) {
            _ = source ?? throw new ArgumentNullException(nameof(source));
            var type = typeof(T);
            var dt = new DataTable();
            var pros = typeof(T).GetProperties();
            foreach(PropertyInfo p in pros) {
                if(p.CanRead) {
                    dt.Columns.Add(new DataColumn {
                        ColumnName = p.Name,
                        DataType = p.PropertyType,
                    });
                }
            }
            foreach(var i in source) {
                var row = dt.NewRow();
                foreach(var p in pros) {
                    if(p.CanRead) {
                        var val = p.GetValue(i);
                        row[p.Name] = val;
                    }
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        /// <summary>
        /// 对枚举进行缩减，并返回缩减后的结果。
        /// </summary>
        /// <typeparam name="T">枚举的类型</typeparam>
        /// <typeparam name="TR">缩减结果类型</typeparam>
        /// <param name="source">原枚举</param>
        /// <param name="reduceFunc">缩减方法</param>
        /// <param name="initialValue">缩减初始值</param>
        /// <returns>缩减结果</returns>
        public static TR Reduce<T, TR>(this IEnumerable<T> source,Func<TR,T,TR> reduceFunc,TR initialValue) {
            if(reduceFunc == null) {
                throw new ArgumentNullException(nameof(reduceFunc));
            }
            var result = initialValue;
            foreach(var i in source) {
                result = reduceFunc(result,i);
            }
            return result;
        }

        /// <summary>
        /// 对枚举进行缩减，并返回缩减后的结果。
        /// </summary>
        /// <typeparam name="T">枚举的类型</typeparam>
        /// <typeparam name="TR">缩减结果类型</typeparam>
        /// <param name="source">原枚举</param>
        /// <param name="reduceFunc">缩减方法</param>
        /// <param name="initialValue">缩减初始值</param>
        /// <returns>缩减结果</returns>
        public static TR Reduce<T, TR>(this IEnumerable<T> source,TR initialValue,Func<TR,T,TR> reduceFunc) {
            if(reduceFunc == null) {
                throw new ArgumentNullException(nameof(reduceFunc));
            }
            var result = initialValue;
            foreach(var i in source) {
                result = reduceFunc(result,i);
            }
            return result;
        }
    }
}

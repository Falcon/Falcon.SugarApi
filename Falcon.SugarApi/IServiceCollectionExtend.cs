﻿using Falcon.SugarApi.ApiDefinistions;
using Falcon.SugarApi.DatabaseDefinitions;
using Falcon.SugarApi.JsonSerialize;
using Falcon.SugarApi.XmlSerialize;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;

namespace Falcon.SugarApi
{
    /// <summary>
    /// 系统服务集合扩展
    /// </summary>
    public static class IServiceCollectionExtend
    {
        /// <summary>
        /// 注册sugarDbcontext数据上下文
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <param name="builder">用于生成配置实例的方法</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddSugarApiDbContext(this IServiceCollection services, Func<IServiceProvider, SugarConnectionConfig> builder) {
            return services.AddSingleton<SugarConnectionConfig>(p => builder(p)).AddTransient<SugarDbContext>();
        }

        /// <summary>
        /// 注册sugarDbcontext数据上下文
        /// </summary>
        /// <param name="service">服务集合</param>
        /// <param name="config">数据库配置</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddSugarApiDbContext(this IServiceCollection service, SugarConnectionConfig config) {
            return service.AddSingleton(config).AddTransient<SugarDbContext>();
        }
        /// <summary>
        /// 注册api返回结果模型提供器
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddApiReturnModelProvider(this IServiceCollection services) {
            services.TryAddEnumerable(ServiceDescriptor.Transient<IApplicationModelProvider, ApiResponseTypeModelProvider>());
            return services;
        }

        /// <summary>
        /// 添加xml序列化工厂XmlSerializeFactory，该工厂可以创建一个IXmlSerialize实现
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddXmlSerializeFactory(this IServiceCollection services) {
            return services.AddSingleton<XmlSerializeFactory>(new XmlSerializeFactory());
        }

        /// <summary>
        /// 添加Json序列化工厂JsonSerializeFactory，该工厂可以创建一个IJsonSerialize实现
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddJsonSerializeFactory(this IServiceCollection services) {
            return services.AddSingleton<JsonSerializeFactory>(new JsonSerializeFactory());
        }
    }
}

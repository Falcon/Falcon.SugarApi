﻿namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// 实体支持修改方法
    /// </summary>
    public interface IModify
    {
        /// <summary>
        /// 更新数据提示
        /// </summary>
        /// <param name="updateBy">修改人</param>
        void Modify(string updateBy);
    }
}
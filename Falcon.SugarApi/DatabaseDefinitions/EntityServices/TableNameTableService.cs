﻿using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Falcon.SugarApi.DatabaseDefinitions.EntityServices
{
    /// <summary>
    /// 设置表名服务
    /// </summary>
    public class TableNameTableService : IEntityTableServices
    {
        /// <inheritdoc/>
        public void SetupTable(Type t, EntityInfo e) {
            if (t.TryGetAttribute<TableAttribute>(out var tn)) {
                e.DbTableName = tn.Name;
            }
        }
    }
}

﻿using SqlSugar;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Falcon.SugarApi.DatabaseDefinitions.EntityServices
{
    /// <summary>
    /// 设置长度规则
    /// </summary>
    public class SetupLengthColumnServices : IEntityColumnServices
    {
        /// <inheritdoc/>
        public void SetupColumn(PropertyInfo p, EntityColumnInfo c) {
            var len = new List<int>();
            if (p.TryGetAttribute<StringLengthAttribute>(out var sl)) {
                len.Add(sl.MaximumLength);
            }
            if (p.TryGetAttribute<MaxLengthAttribute>(out var la)) {
                len.Add(la.Length);
            }
            if (p.TryGetAttribute<SugarColumn>(out var sc)) {
                len.Add(sc.Length);
            }
            if (len.Any()) {
                c.Length = len.Max();
            }
        }
    }
}

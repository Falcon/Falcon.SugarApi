﻿using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Falcon.SugarApi.DatabaseDefinitions.EntityServices
{
    /// <summary>
    /// 设置Nullable
    /// </summary>
    public class SetupNullableColumnServices : IEntityColumnServices
    {
        /// <inheritdoc/>
        public void SetupColumn(PropertyInfo p, EntityColumnInfo c) {
            var pt = p.PropertyType;
            //所有类型默认可空
            bool na = true;
            //字符串默认可空
            na = pt == typeof(string) ? true : na;
            //Nullable<>类型可空
            if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>)) {
                na = true;
            }
            //RequiredAttribute标记不可空
            if (p.GetCustomAttribute<RequiredAttribute>() != null) {
                na = false;
            }
            //主键不可以为空
            if (p.TryGetAttribute<KeyAttribute>(out var _)) {
                na = false;
            }
            //定义主键不可以为空
            if (p.TryGetAttribute<SugarColumn>(out var sc) && sc.IsPrimaryKey) {
                na = false;
            }
            c.IsNullable = na;
        }
    }
}

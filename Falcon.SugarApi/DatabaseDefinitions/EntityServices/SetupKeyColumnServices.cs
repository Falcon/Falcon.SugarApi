﻿using SqlSugar;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Falcon.SugarApi.DatabaseDefinitions.EntityServices
{
    /// <summary>
    /// 设置主键
    /// </summary>
    public class SetupKeyColumnServices : IEntityColumnServices
    {
        /// <inheritdoc/>
        public void SetupColumn(PropertyInfo p, EntityColumnInfo c) {
            if (p.TryGetAttribute<KeyAttribute>(out var _)) {
                c.IsPrimarykey = true;
            }
            //属性只要加了SugarColumn特性，不管是不是增加SugarColumn(IsPrimarykey=false),IsPrimarykey都是false
            //只要存在SugarColumn特性KeyAttribute便会无效。
            //if (p.TryGetAttribute<SugarColumn>(out var sc)) {
            //    c.IsPrimarykey = sc.IsPrimaryKey;
            //}
        }
    }
}

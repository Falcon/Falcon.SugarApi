﻿using Falcon.SugarApi.DatabaseDefinitions.Cache;
using Falcon.SugarApi.DatabaseDefinitions.EntityServices;
using Falcon.SugarApi.JsonSerialize;
using Microsoft.Extensions.Caching.Distributed;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// SqlSugar数据库连接配置
    /// </summary>
    public class SugarConnectionConfig : ConnectionConfig
    {
        /// <summary>
        /// 是否使用log
        /// </summary>
        public bool Log { get; set; }

        /// <summary>
        /// 表映射服务
        /// </summary>
        public static List<IEntityTableServices> TableServices { get; set; } = new List<IEntityTableServices>();
        /// <summary>
        /// 列映射服务
        /// </summary>
        public static List<IEntityColumnServices> ColumnServices { get; set; } = new List<IEntityColumnServices>();

        static SugarConnectionConfig() {
            TableServices.Add(new TableNameTableService());

            ColumnServices.Add(new SetupKeyColumnServices());
            ColumnServices.Add(new SetupLengthColumnServices());
            ColumnServices.Add(new SetupNullableColumnServices());
        }

        /// <summary>
        /// 实例化SugarDb链接配置
        /// </summary>
        public SugarConnectionConfig() {
            this.ConfigureExternalServices ??= new ConfigureExternalServices { };
            this.ConfigureExternalServices.EntityNameService = (t, e) => {
                foreach (var i in TableServices) {
                    i.SetupTable(t, e);
                }
            };
            this.ConfigureExternalServices.EntityService = (p, c) => {
                foreach (var i in ColumnServices) {
                    i.SetupColumn(p, c);
                }
            };
        }

        /// <summary>
        /// 通过配置实现Redis缓冲，必须单例实现.
        /// <para>默认:127.0.0.1:6379,password=,connectTimeout=3000,connectRetry=1,syncTimeout=10000,DefaultDatabase=0</para>
        /// </summary>
        public void AddRedisCache() {
            this.ConfigureExternalServices ??= new ConfigureExternalServices { };
            this.ConfigureExternalServices.DataInfoCacheService = new SqlSugarRedisCache();
        }

        /// <summary>
        /// 通过配置实现Redis缓冲，必须单例实现.
        /// </summary>
        public void AddRedisCache(string connectionString) {
            if (connectionString.IsNullOrEmpty()) {
                AddRedisCache();
                return;
            }
            this.ConfigureExternalServices ??= new ConfigureExternalServices { };
            this.ConfigureExternalServices.DataInfoCacheService = new SqlSugarRedisCache(connectionString);
        }

        /// <summary>
        /// HttpRuntimeCache 缓存
        /// </summary>
        public void AddHttpRuntimeCache() {
            this.ConfigureExternalServices ??= new ConfigureExternalServices { };
            this.ConfigureExternalServices.DataInfoCacheService = new HttpRuntimeCache();
        }

        /// <summary>
        /// 分布式缓存
        /// </summary>
        /// <param name="cache">缓存提供程序</param>
        /// <param name="serialize">序列化实现</param>
        public void AddDistributedCache(IDistributedCache cache, IJsonSerialize serialize) {
            this.ConfigureExternalServices ??= new ConfigureExternalServices { };
            this.ConfigureExternalServices.DataInfoCacheService = new DistributedCache(cache, serialize);
        }
    }
}

﻿using SqlSugar;
using SugarRedis;
using System;
using System.Collections.Generic;

namespace Falcon.SugarApi.DatabaseDefinitions.Cache
{

    /// <summary>
    /// SqlSugar使用Redis缓冲。必须以单例实现
    /// </summary>
    public class SqlSugarRedisCache : ICacheService
    {

        //NUGET安装 SugarRedis  （也可以自个实现）   
        //注意:SugarRedis 不要扔到构造函数里面， 一定要单例模式  


        /// <summary>
        /// 数据缓冲服务
        /// </summary>
        public static SugarRedisClient Service { get; private set; } = null;

        /// <summary>
        /// 默认缓冲时间
        /// </summary>
        public const int DefaultcacheDurationInSeconds = 24 * 60 * 60;

        /// <summary>
        /// 通过配置实现Redis缓冲，必须单例实现.
        /// <para>默认:127.0.0.1:6379,password=,connectTimeout=3000,connectRetry=1,syncTimeout=10000,DefaultDatabase=0</para>
        /// </summary>
        public SqlSugarRedisCache() {
            Service ??= new SugarRedisClient();
        }

        /// <summary>
        /// 通过配置实现Redis缓冲，必须单例实现
        /// </summary>
        /// <param name="redisConnectionString">Redis链接字符串</param>
        public SqlSugarRedisCache(string redisConnectionString) {
            Service ??= redisConnectionString.IsNullOrEmpty() ? new SugarRedisClient()
                : new SugarRedisClient(redisConnectionString);
        }

        public void Add<V>(string key, V value) {
            Service.Set(key, value);
        }

        public void Add<V>(string key, V value, int cacheDurationInSeconds) {
            cacheDurationInSeconds = Math.Min(cacheDurationInSeconds, DefaultcacheDurationInSeconds);
            Service.Set(key, value, cacheDurationInSeconds / 60);
        }

        public bool ContainsKey<V>(string key) {
            return Service.Exists(key);
        }

        public V Get<V>(string key) {
            return Service.Get<V>(key);
        }

        public IEnumerable<string> GetAllKey<V>() {

            return Service.SearchCacheRegex("SqlSugarDataCache.*");
        }

        public V GetOrCreate<V>(string cacheKey, Func<V> create, int cacheDurationInSeconds = DefaultcacheDurationInSeconds) {
            if (ContainsKey<V>(cacheKey)) {
                var result = Get<V>(cacheKey);
                if (result == null) {
                    return create();
                }
                else {
                    return result;
                }
            }
            else {
                var result = create();
                Add(cacheKey, result, cacheDurationInSeconds);
                return result;
            }
        }

        public void Remove<V>(string key) {
            Service.Remove(key);
        }
    }
}

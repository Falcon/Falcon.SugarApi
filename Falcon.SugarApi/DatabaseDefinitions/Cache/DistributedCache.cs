﻿using Falcon.SugarApi.JsonSerialize;
using Microsoft.Extensions.Caching.Distributed;
using SqlSugar;
using System;
using System.Collections.Generic;

namespace Falcon.SugarApi.DatabaseDefinitions.Cache
{
    /// <summary>
    /// 使用分布式缓存存储器
    /// </summary>
    public class DistributedCache : ICacheService
    {
        /// <summary>
        /// 分布式缓存存储器
        /// </summary>
        public IDistributedCache Cache { get; set; }

        /// <summary>
        /// 序列化方法
        /// </summary>
        public IJsonSerialize Serialize { get; set; }

        /// <summary>
        /// 提供分布式缓存存储器
        /// </summary>
        /// <param name="cache">分布式缓存存储器</param>
        /// <param name="serialize">实现序列化的接口s</param>
        public DistributedCache(IDistributedCache cache, IJsonSerialize serialize) {
            this.Cache = cache;
            this.Serialize = serialize;
        }

        /// <inheritdoc/>
        public void Add<V>(string key, V value) {
            var valStr = this.Serialize.Serialize(value);
            this.Cache.SetString(key, valStr);
        }

        /// <inheritdoc/>
        public void Add<V>(string key, V value, int cacheDurationInSeconds) {
            var valStr = this.Serialize.Serialize(value);
            DistributedCacheEntryOptions op = new() {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(cacheDurationInSeconds),
            };
            this.Cache.SetString(key, valStr, op);
        }

        /// <inheritdoc/>
        public bool ContainsKey<V>(string key) {
            return this.Cache.Get(key) != null;
        }

        /// <inheritdoc/>
        public V Get<V>(string key) {
            var val = this.Cache.GetString(key);
            if (val == null) return default(V);
            return (V)this.Serialize.Deserialize(val, typeof(V));
        }

        /// <inheritdoc/>
        public IEnumerable<string> GetAllKey<V>() {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public V GetOrCreate<V>(string cacheKey, Func<V> create, int cacheDurationInSeconds = int.MaxValue) {
            if (ContainsKey<V>(cacheKey)) {
                return Get<V>(cacheKey);
            }
            V val = create();
            Add(cacheKey, val, cacheDurationInSeconds);
            return val;
        }

        /// <inheritdoc/>
        public void Remove<V>(string key) {
            this.Cache.Remove(key);
        }
    }
}

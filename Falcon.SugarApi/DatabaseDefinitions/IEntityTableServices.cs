﻿using SqlSugar;
using System;
using System.Reflection;

namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// 实现实体类型到表映射服务
    /// </summary>
    public interface IEntityTableServices
    {
        /// <summary>
        /// 设置表属性
        /// </summary>
        /// <param name="t">类型</param>
        /// <param name="e">实体</param>
        void SetupTable(Type t, EntityInfo e);
    }
}

﻿namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// 实体类支持新建数据方法
    /// </summary>
    public interface ICreateNew
    {
       /// <summary>
       /// 创建新数据实体
       /// </summary>
       /// <param name="createBy">创建人</param>
        void CreateNew(string createBy);
    }
}
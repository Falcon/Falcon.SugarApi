﻿using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations;

namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// 基础表.只定义表基础主键和创建时间
    /// </summary>
    public abstract class SugarBasicTable
    {
        /// <summary>
        /// 主键
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, ColumnDescription = "主键")]
        public Guid Id { get; set; } = Guid.NewGuid();
        /// <summary>
        /// 创建时间
        /// </summary>
        [Required]
        [SugarColumn(IsOnlyIgnoreUpdate = true, ColumnDescription = "创建时间")]
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 将数据设置为新数据。
        /// <para>主要更新Id、创建时间和记录状态</para>
        /// </summary>
        /// <param name="now">当前时间，如果采用非本机时间可以提供</param>
        /// <returns>本条数据</returns>
        public virtual SugarBasicTable SetNew(DateTime? now = null) {
            this.Id = Guid.NewGuid();
            this.CreateTime = now ?? DateTime.Now;
            return this;
        }
    }
}

﻿using SqlSugar;
using System;
using System.Reflection;

namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// 表示数据库表结构定义
    /// </summary>
    public abstract class SugarDbTables
    {
        /// <summary>
        /// 表使用的数据上下文
        /// </summary>
        private SugarDbContext DbContext { get; set; }

        /// <summary>
        /// 通过数据上下文构造表集合对象，并实例化所有DbSet实例
        /// </summary>
        /// <param name="dbContext">使用的数据上下文</param>
        /// <param name="createInstance">是否实例化所有DbSet表对象</param>
        public SugarDbTables(SugarDbContext dbContext, bool createInstance = true) {
            this.DbContext = dbContext;
            if (createInstance) {
                foreach (PropertyInfo property in this.GetType().GetProperties()) {
                    var ptype = property.PropertyType;
                    if (ptype.IsGenericType() && ptype.GetGenericTypeDefinition() == typeof(DbSet<>) && property.CanWrite) {
                        property.SetValue(this, Activator.CreateInstance(ptype, this.DbContext));
                    }
                }
            }
        }
    }

}

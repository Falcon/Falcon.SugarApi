﻿namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// 实体软删除方法
    /// </summary>
    public interface IDelete
    {
        /// <summary>
        /// 删除本条数据。软删除
        /// </summary>
        /// <param name="deleteBy">删除人</param>
        void Delete(string deleteBy);
    }
}
﻿using SqlSugar;
using System.Reflection;

namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// 实现实体属性到列的映射服务
    /// </summary>
    public interface IEntityColumnServices
    {
        /// <summary>
        /// 设置列属性
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        void SetupColumn(PropertyInfo p, EntityColumnInfo c);
    }
}

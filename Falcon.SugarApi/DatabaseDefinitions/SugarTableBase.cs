﻿using SqlSugar;
using System;
using System.Reflection;

namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// 负载表基类。一般表应该继承此类，包括SugarBasicTable定义，和修改人，行状态,以及一些相关方法等
    /// </summary>
    public abstract class SugarTableBase :SugarBasicTable, ICreateNew, IModify, IDelete
    {
        /// <summary>
        /// 创建人
        /// </summary>
        [SugarColumn(IsOnlyIgnoreUpdate = true, IsNullable = true, ColumnDescription = "首次创建人")]
        public string? CreateBy { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "最近更新人")]
        public string? UpdateBy { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "最新更新时间")]
        public DateTime? UpdateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 记录状态
        /// </summary>
        [SugarColumn(IsNullable = false, ColumnDescription = "记录状态。有效 无效")]
        public string Status { get; set; } = "有效";

        /// <summary>
        /// 创建新数据记录。
        /// </summary>
        /// <param name="createBy">创建人</param>
        public virtual void CreateNew(string createBy) {
            base.SetNew();
            this.CreateBy = createBy;
            this.UpdateBy = this.CreateBy;
            this.UpdateTime = this.CreateTime;
            this.Status = RecordStetus.Effective;
        }

        /// <summary>
        /// 修改实体数据
        /// </summary>
        /// <param name="updateBy">修改人</param>
        public virtual void Modify(string updateBy) {
            this.UpdateBy = updateBy;
            this.UpdateTime = DateTime.Now;
        }
        /// <summary>
        /// 删除本条数据。软删除
        /// </summary>
        /// <param name="deleteBy">删除人</param>
        public virtual void Delete(string deleteBy) {
            this.UpdateBy = deleteBy;
            this.UpdateTime = DateTime.Now;
            this.Status = RecordStetus.Invalid;
        }

        /// <summary>
        /// 将实体设置为测试用数据!生成测试数据将丢失原有数据，只在测试情况下使用
        /// </summary>
        public virtual SugarTableBase SetTestModel() {
            foreach (PropertyInfo pro in this.GetType().GetProperties()) {
                if (pro.Name == "Id") {
                    this.Id = Guid.NewGuid();
                    continue;
                }
                if (pro.Name == "CreateBy" || pro.Name == "UpdateBy") {
                    pro.SetValue(this, "admin");
                    continue;
                }
                if (pro.Name == "CreateTime" || pro.Name == "UpdateTime") {
                    pro.SetValue(this, DateTime.Now);
                    continue;
                }
                if (pro.Name == "Status") {
                    this.Status = "Effective";
                    continue;
                }
                if (pro.CanWrite && pro.PropertyType == typeof(string)) {
                    pro.SetValue(this, pro.Name);
                    continue;
                }
            }
            return this;
        }

        /// <summary>
        /// 设置记录有效
        /// </summary>
        public virtual SugarTableBase SetEnable() {
            this.Status = RecordStetus.Effective;
            return this;
        }
        /// <summary>
        /// 设置记录无效
        /// </summary>
        public virtual SugarTableBase SetDisable() {
            this.Status = RecordStetus.Invalid;
            return this;
        }

    }
}

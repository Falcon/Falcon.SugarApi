﻿using SqlSugar;

namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// 代表一张表
    /// </summary>
    /// <typeparam name="T">表定义类型</typeparam>
    public class DbSet<T> : SimpleClient<T> where T : class, new()
    {
        /// <summary>
        /// 通过数据上下文构造一张表
        /// </summary>
        /// <param name="context">数据上下文</param>
        public DbSet(SqlSugarClient context) : base(context) { }
        /// <summary>
        /// 返回可迭代的
        /// </summary>
        /// <returns></returns>
        public virtual ISugarQueryable<T> Queryable() {
            return Context.Queryable<T>();
        }
    }

}

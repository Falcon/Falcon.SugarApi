﻿namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// 记录状态
    /// </summary>
    public static class RecordStetus
    {
        /// <summary>
        /// 有效记录
        /// </summary>
        public static string Effective => "有效";
        /// <summary>
        /// 无效，已删除记录
        /// </summary>
        public static string Invalid => "无效";
    }
}

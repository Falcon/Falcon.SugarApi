﻿namespace Falcon.SugarApi.DatabaseDefinitions
{
    /// <summary>
    /// 创建数据库表
    /// </summary>
    public interface ICreateDbTable
    {
        /// <summary>
        /// 创建数据库相关表
        /// </summary>
        /// <returns>创建结果</returns>
        string CreateTable();
    }
}

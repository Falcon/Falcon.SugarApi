﻿using System;

namespace Falcon.SugarApi
{
    /// <summary>
    /// 序列化对象到字符串
    /// </summary>
    public interface ISerialize
    {
        /// <summary>
        /// 序列化对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="obj">对象</param>
        /// <returns>序列化字符串</returns>
        public string Serialize<T>(T obj);

        /// <summary>
        /// 反序列化对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="str">序列化字符串</param>
        /// <returns>对象实例</returns>
        public T? Deserialize<T>(string str) where T : class;

        /// <summary>
        /// 反序列化json字符串
        /// </summary>
        /// <param name="str">json字符串</param>
        /// <param name="returnType">返回类型</param>
        /// <returns>json对象</returns>
        public object? Deserialize(string str, Type returnType);

        /// <summary>
        /// 序列化json对象
        /// </summary>
        /// <param name="obj">json对象</param>
        /// <param name="inputType">输入类型</param>
        /// <returns>json字符串</returns>
        public string Serialize(object obj, Type inputType);

    }
}

﻿using System;
using System.Xml;

namespace Falcon.SugarApi.XmlSerialize
{
    /// <summary>
    /// XmlWriter扩展。实现完整的扩展标记
    /// </summary>
    /// <remarks>接口要求书写完整的结束标记，但是默认XmlWriter使用/结束空标记</remarks>
    public class MyXmlWriter : XmlWriter, IDisposable
    {
        /// <summary>
        /// 基础XmlWriter
        /// </summary>
        public XmlWriter BaseWriter { get; set; }
        /// <summary>
        /// 序列化设置
        /// </summary>
        public XmlSerializeSettings MySettings { get; set; }
        /// <summary>
        /// 通过基础序列化器和序列化设置实例化
        /// </summary>
        /// <param name="writer">基础序列化器</param>
        /// <param name="settings">序列化设置</param>
        public MyXmlWriter(XmlWriter writer, XmlSerializeSettings settings) {
            this.BaseWriter = writer;
            this.MySettings = settings;
        }

#pragma warning disable CS1591 // 缺少对公共可见类型或成员的 XML 注释
        #region XmlWriter实现
        public override WriteState WriteState => BaseWriter.WriteState;

        public override void Flush() => this.BaseWriter.Flush();

        public override string? LookupPrefix(string ns) => BaseWriter.LookupPrefix(ns);

        public override void WriteBase64(byte[] buffer, int index, int count) => this.BaseWriter.WriteBase64(buffer, index, count);

        public override void WriteCData(string? text) => this.BaseWriter.WriteCData(text);

        public override void WriteCharEntity(char ch) => this.BaseWriter.WriteCharEntity(ch);

        public override void WriteChars(char[] buffer, int index, int count) => this.BaseWriter.WriteChars(buffer, index, count);

        public override void WriteComment(string? text) => this.BaseWriter.WriteComment(text);

        public override void WriteDocType(string name, string? pubid, string? sysid, string? subset) => this.BaseWriter.WriteDocType(name, pubid, sysid, subset);

        public override void WriteEndAttribute() => this.BaseWriter.WriteEndAttribute();

        /// <summary>
        /// 写入完整的关闭节点
        /// </summary>
        public override void WriteEndElement() {
            if (this.MySettings.WriteFullEndElement) {
                BaseWriter.WriteFullEndElement();
            }
            else {
                BaseWriter.WriteEndElement();
            }
        }

        public override void WriteFullEndElement() => BaseWriter.WriteFullEndElement();

        public override void Close() => BaseWriter.Close();

        public override void WriteEndDocument() => BaseWriter.WriteEndDocument();

        public override void WriteEntityRef(string name) => BaseWriter.WriteEntityRef(name);

        public override void WriteProcessingInstruction(string name, string? text) => BaseWriter.WriteProcessingInstruction(name, text);

        public override void WriteRaw(string data) => BaseWriter.WriteRaw(data);

        public override void WriteRaw(char[] buffer, int index, int count) => BaseWriter.WriteRaw(buffer, index, count);

        public override void WriteStartAttribute(string? prefix, string localName, string? ns) => BaseWriter.WriteStartAttribute(prefix, localName, ns);

        public override void WriteStartDocument(bool standalone) => BaseWriter.WriteStartDocument(standalone);

        public override void WriteStartDocument() => BaseWriter.WriteStartDocument();

        public override void WriteStartElement(string? prefix, string localName, string? ns) => BaseWriter.WriteStartElement(prefix, localName, ns);

        public override void WriteString(string? text) => BaseWriter.WriteString(text);

        public override void WriteSurrogateCharEntity(char lowChar, char highChar) => BaseWriter.WriteSurrogateCharEntity(lowChar, highChar);

        public override void WriteWhitespace(string? ws) => BaseWriter.WriteWhitespace(ws);

        #endregion
#pragma warning restore CS1591 // 缺少对公共可见类型或成员的 XML 注释

        /// <summary>
        /// 释放占用资源
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing) {
            if (disposing) {
                this.BaseWriter.Dispose();
            }
            base.Dispose(disposing);
        }

    }

}

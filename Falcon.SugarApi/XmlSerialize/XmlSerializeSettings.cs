﻿using System.Collections.Generic;
using System.Text;

namespace Falcon.SugarApi.XmlSerialize
{
    /// <summary>
    /// XML序列化设置
    /// </summary>
    public class XmlSerializeSettings
    {
        /// <summary>
        /// 编写XML声明。默认false
        /// </summary>
        public bool OmitXmlDeclaration { set; get; } = false;
        /// <summary>
        /// XML编码格式。默认Encoding.UTF8
        /// </summary>
        public Encoding Encoding { set; get; } = Encoding.UTF8;
        /// <summary>
        /// 是否换行缩进。默认true
        /// </summary>
        public bool Indent { set; get; } = true;
        /// <summary>
        /// 缩进使用字符串。默认"  "
        /// </summary>
        public string IndentChars { set; get; } = "  ";
        /// <summary>
        /// XML名字空间。默认空
        /// </summary>
        public List<KeyValuePair<string, string>> Namespaces { get; set; } = new();
        /// <summary>
        /// 对空节点使用完整的结束标记。默认true
        /// </summary>
        public bool WriteFullEndElement { get; set; } = true;
    }

}

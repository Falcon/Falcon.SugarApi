﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Falcon.SugarApi.XmlSerialize
{
    /// <summary>
    /// XML序列化实现
    /// </summary>
    public class XmlSerialize : IXmlSerialize
    {
        /// <summary>
        /// 序列化设置
        /// </summary>
        public XmlSerializeSettings Settings { get; set; }

        /// <summary>
        /// 使用默认配置的序列化器
        /// </summary>
        public XmlSerialize() : this(new XmlSerializeSettings()) { }
        /// <summary>
        /// 使用特定配置的序列化器
        /// </summary>
        /// <param name="settings">序列化配置</param>
        public XmlSerialize(XmlSerializeSettings settings) {
            this.Settings = settings;
        }

        /// <summary>
        /// 反序列化对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="str">序列化字符串</param>
        /// <returns>对象实例</returns>
        public T? Deserialize<T>(string str) where T : class {
            var serializer = new XmlSerializer(typeof(T));
            using var ms = new MemoryStream(Encoding.UTF8.GetBytes(str));
            var obj = serializer.Deserialize(ms) as T;
            return obj;
        }

        /// <summary>
        /// 序列化对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="obj">对象</param>
        /// <returns>序列化字符串</returns>
        public string Serialize<T>(T obj) {
            return Serialize(obj, typeof(T));
        }

        /// <inheritdoc/>
        public object? Deserialize(string str, Type returnType) {
            var serializer = new XmlSerializer(returnType);
            using var ms = new MemoryStream(Encoding.UTF8.GetBytes(str));
            return serializer.Deserialize(ms);
        }

        /// <inheritdoc/>
        public string Serialize(object obj, Type inputType) {
            var xmlSerializer = new XmlSerializer(inputType);
            using var ms = new MemoryStream();
            XmlSerializerNamespaces xmlSerializerNamespaces = new();
            if (this.Settings.Namespaces.Count > 0) {
                foreach (var item in this.Settings.Namespaces) {
                    xmlSerializerNamespaces.Add(item.Key, item.Value);
                }
            }
            else {
                xmlSerializerNamespaces.Add("", "");
            }
            var settings = new XmlWriterSettings {
                OmitXmlDeclaration = this.Settings.OmitXmlDeclaration,
                Encoding = this.Settings.Encoding,
                Indent = this.Settings.Indent,
                IndentChars = this.Settings.IndentChars,
            };
            using XmlWriter xmlWriter = new MyXmlWriter(XmlWriter.Create(ms, settings), this.Settings);
            xmlSerializer.Serialize(xmlWriter, obj, xmlSerializerNamespaces);
            ms.Position = 0;
            using var sr = new StreamReader(ms, Encoding.UTF8);
            return sr.ReadToEnd();
        }
    }

}

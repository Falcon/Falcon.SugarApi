﻿using System;

namespace Falcon.SugarApi.XmlSerialize
{
    /// <summary>
    /// xml序列化器工厂
    /// </summary>
    public class XmlSerializeFactory
    {
        /// <summary>
        /// 使用默认设置创建xml序列化器
        /// </summary>
        /// <returns>序列化器</returns>
        public IXmlSerialize CreateXmlSerialize() {
            return new XmlSerialize();
        }
        /// <summary>
        /// 使用特定设置创建xml序列化器
        /// </summary>
        /// <param name="setter">特定设置</param>
        /// <returns>序列化器</returns>
        public IXmlSerialize CreateXmlSerialize(Action<XmlSerializeSettings> setter) {
            var settings = new XmlSerializeSettings { };
            setter(settings);
            return new XmlSerialize(settings);
        }
    }
}

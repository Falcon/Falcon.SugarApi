﻿using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Falcon.SugarApi.Swagger
{
    /// <summary>
    /// SwaggerGenOptions扩展
    /// </summary>
    public static class SwaggerGenOptionsExtend
    {
        /// <summary>
        /// 增加xml文件枚举说明支持
        /// </summary>
        /// <param name="options">SwaggerGenOptions选项</param>
        /// <param name="xmlPath">XML文件路径</param>
        /// <returns>SwaggerGenOptions</returns>
        public static SwaggerGenOptions AddXmlEnumEnable(this SwaggerGenOptions options,string xmlPath) {
            options.DocumentFilter<SwaggerXmlEnumFilter>(xmlPath);
            return options;
        }
    }
}

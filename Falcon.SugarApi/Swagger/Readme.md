﻿## 使用SwaggerGenOptions.AddXmlEnumEnable方法可以为枚举类型增加matedata说明

```c#
    //注册Swagger
    services.AddSwaggerGen(c => {
        //获取应用程序基础目录
        var basePath = AppContext.BaseDirectory;
        //指定应用程序说明XML文件
        var xmlFile = new string[] { "ggws.Service.xml", "Ggws.Database.xml", "Falcon.SugarApi.xml" };
        foreach (var xf in xmlFile) {
            var path = Path.Combine(basePath, xf);
            //引入文件
            c.IncludeXmlComments(path, true);
            //为枚举增加说明
            c.AddXmlEnumEnable(path);
        }
    });
    
```
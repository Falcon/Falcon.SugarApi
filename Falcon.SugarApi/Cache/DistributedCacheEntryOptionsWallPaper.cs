﻿using System;
using Microsoft.Extensions.Caching.Distributed;

namespace Falcon.SugarApi.Cache
{
    /// <summary>
    /// 分布式缓冲选项
    /// </summary>
    internal class DistributedCacheEntryOptionsWallPaper : DistributedCacheEntryOptions
    {
        /// <summary>
        /// 通过过期时间段实例化DistributedCacheEntryOptions
        /// </summary>
        /// <param name="timeSpan">时间段</param>
        public DistributedCacheEntryOptionsWallPaper(TimeSpan timeSpan) {
            this.AbsoluteExpirationRelativeToNow = timeSpan;
        }
    }
}

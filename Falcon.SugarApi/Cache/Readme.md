﻿## 缓冲器
> 使用 `IServiceCollection.AddSugarRedisCache` 或 `IServiceCollection.AddSugarMemoryCache` 注册缓冲器即可
> 在使用时注入 `ISugarCache`同步接口 或 `ISugarCacheAsync`异步接口
> 接口的`Get`和`Set`方法分别对应获取和保存缓冲值。

> 在可能的情况下尽量使用异步方法，异步效率远远高于同步方法。
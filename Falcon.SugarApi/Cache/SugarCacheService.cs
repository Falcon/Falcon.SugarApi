﻿using Falcon.SugarApi.JsonSerialize;
using SqlSugar;
using System;
using System.Collections.Generic;

namespace Falcon.SugarApi.Cache
{
    /// <summary>
    /// SugarCache的ICacheService实现
    /// </summary>
    public class SugarCacheService : ICacheService
    {
        /// <summary>
        /// 默认缓存时间
        /// </summary>
        public int DefaultcacheDurationInSeconds { get; set; } = 24 * 60 * 60;

        /// <summary>
        /// 默认缓冲时间
        /// </summary>
        protected TimeSpan DefaultTimeSpan => TimeSpan.FromSeconds(DefaultcacheDurationInSeconds);

        /// <summary>
        /// 构造ICacheService实现
        /// </summary>
        public SugarCacheService(ISugarCache cache, IJsonSerialize json) {
            Cache = cache;
            Json = json;
        }

        /// <summary>
        /// 缓冲数据提供器
        /// </summary>
        public ISugarCache Cache { get; }
        /// <summary>
        /// json序列化
        /// </summary>
        public IJsonSerialize Json { get; }

        /// <inheritdoc/>
        public void Add<V>(string key, V value) => Add(key, value, DefaultcacheDurationInSeconds);

        /// <inheritdoc/>
        public void Add<V>(string key, V value, int cacheDurationInSeconds) {
            if (value == null) {
                return;
            }
            var str = Json.Serialize(value);
            Cache.SetStringValue(key, str, TimeSpan.FromSeconds(cacheDurationInSeconds));
        }

        /// <inheritdoc/>
        public bool ContainsKey<V>(string key) {
            return Get<V>(key) != null;
        }

        /// <inheritdoc/>
        public V Get<V>(string key) {
            _ = key ?? throw new ArgumentNullException(nameof(key));
            var str = this.Cache.GetStringValue(key);
            return str.IsNullOrEmpty() ? default(V) : (V)Json.Deserialize(str, typeof(V));
        }

        /// <inheritdoc/>
        public IEnumerable<string> GetAllKey<V>() {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public V GetOrCreate<V>(string cacheKey, Func<V> create, int cacheDurationInSeconds = int.MaxValue) {
            var val = Get<V>(cacheKey) ?? create();
            Add(cacheKey, val);
            return val;
        }

        /// <inheritdoc/>
        public void Remove<V>(string key) => this.Cache.SetStringValue(key, null, DefaultTimeSpan);
    }
}

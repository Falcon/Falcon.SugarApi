﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Falcon.SugarApi.Cache
{
    /// <summary>
    /// 提供数据缓冲支持
    /// </summary>
    public interface ISugarCache
    {
        /// <summary>
        /// 获取缓冲对象
        /// </summary>
        /// <param name="key">缓存键</param>
        /// <returns></returns>
        T? GetValue<T>([NotNull] string key) where T : class;

        /// <summary>
        /// 获取缓存的字符串值
        /// </summary>
        /// <param name="key">缓存键</param>
        /// <returns>缓冲值</returns>
        string GetStringValue([NotNull] string key);

        /// <summary>
        /// 设置对象缓冲
        /// </summary>
        /// <param name="key">缓冲键</param>
        /// <param name="value">缓冲值</param>
        /// <param name="cacheTimeSpan">缓冲时长</param>
        void SetValue<T>([NotNull] string key, T value, TimeSpan cacheTimeSpan) where T : class;

        /// <summary>
        /// 缓存字符串值
        /// </summary>
        /// <param name="key">缓存键</param>
        /// <param name="value">缓冲值</param>
        /// <param name="cacheTimeSpan">缓冲时长</param>
        void SetStringValue([NotNull] string key, string value, TimeSpan cacheTimeSpan);
    }
}

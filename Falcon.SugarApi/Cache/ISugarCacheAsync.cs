﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace Falcon.SugarApi.Cache
{
    /// <summary>
    /// 提供数据缓冲支持
    /// </summary>
    public interface ISugarCacheAsync
    {
        /// <summary>
        /// 获取字符串换重置
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<T?> GetValueAsync<T>([NotNull] string key, CancellationToken cancellationToken = default) where T : class;

        /// <summary>
        /// 获取字符串换重置
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<string?> GetStringValueAsync([NotNull] string key, CancellationToken cancellationToken = default);

        /// <summary>
        /// 设置字符串缓冲
        /// </summary>
        /// <param name="key">缓冲键</param>
        /// <param name="value">缓冲值</param>
        /// <param name="cacheTimeSpan">缓冲时长</param>
        /// <param name="cancellationToken"></param>
        Task SetValueAsync<T>([NotNull] string key, T value, TimeSpan cacheTimeSpan, CancellationToken cancellationToken = default) where T : class;

        /// <summary>
        /// 设置字符串缓冲
        /// </summary>
        /// <param name="key">缓冲键</param>
        /// <param name="value">缓冲值</param>
        /// <param name="cacheTimeSpan">缓冲时长</param>
        /// <param name="cancellationToken"></param>
        Task SetStringValueAsync([NotNull] string key, string value, TimeSpan cacheTimeSpan, CancellationToken cancellationToken = default);
    }
}

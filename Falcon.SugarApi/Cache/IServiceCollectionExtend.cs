﻿using Falcon.SugarApi.JsonSerialize;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.DependencyInjection;
using SqlSugar;
using System;

namespace Falcon.SugarApi.Cache
{
    /// <summary>
    /// 服务集合扩展
    /// </summary>
    public static class IServiceCollectionExtend
    {
        /// <summary>
        /// 注册Redis缓冲服务器
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <param name="configRedis">缓冲配置</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddSugarRedisCache(this IServiceCollection services, Action<RedisCacheOptions> configRedis) {
            services.AddSingleton<JsonSerializeFactory>(_ => new JsonSerializeFactory());
            services.AddStackExchangeRedisCache(configRedis);
            services.AddSingleton<ISugarCache, SugarCache>();
            services.AddSingleton<ISugarCacheAsync, SugarCache>();
            services.AddSingleton<ICacheService, SugarCacheService>();
            return services;
        }

        /// <summary>
        /// 注册内存缓冲服务器
        /// </summary>
        /// <param name="services">服务结合</param>
        /// <param name="configAction">缓冲配置</param>
        /// <returns>服务结合</returns>
        public static IServiceCollection AddSugarMemoryCache(this IServiceCollection services, Action<MemoryDistributedCacheOptions> configAction) {
            services.AddSingleton<JsonSerializeFactory>(_ => new JsonSerializeFactory());
            services.AddDistributedMemoryCache(configAction);
            services.AddSingleton<ISugarCache, SugarCache>();
            services.AddSingleton<ISugarCacheAsync, SugarCache>();
            services.AddSingleton<ICacheService, SugarCacheService>();
            return services;
        }
    }
}

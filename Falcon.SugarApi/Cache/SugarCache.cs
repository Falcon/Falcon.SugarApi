﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Falcon.SugarApi.JsonSerialize;
using Microsoft.Extensions.Caching;
using Microsoft.Extensions.Caching.Distributed;

namespace Falcon.SugarApi.Cache
{
    /// <summary>
    /// 缓冲器
    /// </summary>
    public class SugarCache : ISugarCache, ISugarCacheAsync
    {
        /// <summary>
        /// 内部缓冲提供器实现
        /// </summary>
        public IDistributedCache CacheProvider { get; set; }
        /// <summary>
        /// 序列化提供器
        /// </summary>
        public IJsonSerialize Json { get; }

        /// <summary>
        /// 构造缓冲器
        /// </summary>
        /// <param name="cache">基础分布式缓冲器</param>
        /// <param name="factory">Json序列化工厂</param>
        public SugarCache(IDistributedCache cache, JsonSerializeFactory factory) {
            this.CacheProvider = cache;
            Json = factory.CreateJsonSerialize();
        }

        /// <inheritdoc/>
        public T? GetValue<T>([NotNull] string key) where T : class {
            var str = GetStringValue(key);
            return str.IsNullOrEmpty() ? null : Json.Deserialize<T>(str);
        }
        /// <inheritdoc/>
        public string GetStringValue([NotNull] string key) {
            return this.CacheProvider.GetString(key);
        }
        /// <inheritdoc/>
        public void SetValue<T>([NotNull] string key, T value, TimeSpan cacheTimeSpan) where T : class {
            if (value == null) {
                CacheProvider.Remove(key);
                return;
            }
            var str = Json.Serialize(value);
            CacheProvider.SetString(key, str);
        }

        /// <inheritdoc/>
        public void SetStringValue([NotNull] string key, string value, TimeSpan cacheTimeSpan) {
            CacheProvider.SetString(key, value, new DistributedCacheEntryOptionsWallPaper(cacheTimeSpan));
        }

        /// <inheritdoc/>
        public async Task<T?> GetValueAsync<T>([NotNull] string key, CancellationToken cancellationToken = default) where T : class {
            var str = await CacheProvider.GetStringAsync(key, cancellationToken);
            if (str.IsNullOrEmpty()) {
                return null;
            }
            return Json.Deserialize<T>(str);
        }

        /// <inheritdoc/>
        public async Task<string?> GetStringValueAsync([NotNull] string key, CancellationToken cancellationToken = default) {
            return await CacheProvider.GetStringAsync(key, cancellationToken);
        }

        /// <inheritdoc/>
        public async Task SetValueAsync<T>([NotNull] string key, T value, TimeSpan cacheTimeSpan, CancellationToken cancellationToken = default) where T : class {
            if (value == null) {
                await CacheProvider.RemoveAsync(key, cancellationToken);
                return;
            }
            var str = Json.Serialize(value);
            await CacheProvider.SetStringAsync(key, str, cancellationToken);
        }

        /// <inheritdoc/>
        public async Task SetStringValueAsync([NotNull] string key, string value, TimeSpan cacheTimeSpan, CancellationToken cancellationToken = default) {
            if (value == null) {
                await CacheProvider.RemoveAsync(key, cancellationToken);
                return;
            }
            await CacheProvider.SetStringAsync(key, value, cancellationToken);
        }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Falcon.SugarApi.BackTask
{
    /// <summary>
    /// 服务集合方法扩展
    /// </summary>
    public static class IServiceCollectionExtend
    {
        /// <summary>
        /// 注册BackgroundLongTask后台任务。
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddBackgroundLongTask<T>(this IServiceCollection services)
            where T : BackgroundLongTask {
            return services.AddHostedService<T>();
        }
    }
}

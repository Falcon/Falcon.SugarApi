﻿using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Falcon.SugarApi.BackTask
{
    /// <summary>
    /// 长时间执行的后台任务
    /// </summary>
    public abstract class BackgroundLongTask : BackgroundService, IDisposable
    {
        /// <summary>
        /// 要执行的任务，异步。
        /// </summary>
        /// <returns>继续执行True，退出False</returns>
        protected virtual Task<bool> RunAsync() { return Task.FromResult(false); }
        /// <summary>
        /// 要执行的任务，同步。
        /// </summary>
        /// <returns>继续执行True，退出False</returns>
        protected virtual bool Run() { return false; }
        /// <summary>
        /// 执行时间间隔,秒
        /// </summary>
        public abstract float RunTimespan { get; }
        /// <summary>
        /// 后台任务开始
        /// </summary>
        protected virtual void OnStart(BackgroundLongTask t) { }
        /// <summary>
        /// 后台任务停止
        /// </summary>
        protected virtual void OnStop(BackgroundLongTask t) { }
        /// <summary>
        /// 完成一次执行
        /// </summary>
        protected virtual void OnCompleted(BackgroundLongTask t) { }
        /// <summary>
        /// 执行中发生未处理异常
        /// </summary>
        protected virtual void OnException(BackgroundLongTask t, Exception ex) { }

        /// <summary>
        /// 用于定期执行任务的委托,异步
        /// </summary>
        private Action actionAsync;
        /// <summary>
        /// 用于定期执行任务的委托,同步
        /// </summary>
        private Action action;

        private CancellationTokenSource TokenSource { get; set; } = new();

        /// <summary>
        /// 构造一个后台长期任务
        /// </summary>
        public BackgroundLongTask() {
            actionAsync = async () => {
                OnStart(this);
                while (!TokenSource.Token.IsCancellationRequested) {
                    await Task.Delay(TimeSpan.FromSeconds(RunTimespan))
                    .ContinueWith(async _ => {
                        try {
                            if (!await RunAsync()) {
                                TokenSource.Cancel();
                            }
                        }
                        catch (Exception ex) {
                            OnException(this, ex);
                        }
                    })
                    .ContinueWith(t => { OnCompleted(this); });
                }
                OnStop(this);
            };
            action = () => {
                OnStart(this);
                while (true) {
                    Thread.Sleep(TimeSpan.FromSeconds(RunTimespan));
                    try {
                        if (!Run()) break;
                        OnCompleted(this);
                    }
                    catch (Exception ex) {
                        OnException(this, ex);
                    }
                }
                OnStop(this);
            };
        }
        /// <summary>
        /// 服务器启动时执行
        /// </summary>
        /// <param name="stoppingToken">退出信号</param>
        /// <returns></returns>
        protected override Task ExecuteAsync(CancellationToken stoppingToken) {
            _ = Task.Factory.StartNew(actionAsync, stoppingToken);
            _ = Task.Factory.StartNew(action, stoppingToken);
            return Task.CompletedTask;
        }
        /// <summary>
        /// 停止任务
        /// </summary>
        /// <param name="cancellationToken">退出标记</param>
        /// <returns>任务</returns>
        public override Task StopAsync(CancellationToken cancellationToken) {
            this.TokenSource.Cancel();
            return base.StopAsync(cancellationToken);
        }

        /// <summary>
        /// 释放空间，结束任务
        /// </summary>
        public override void Dispose() {
            this.TokenSource.Cancel();
            base.Dispose();
        }
    }
}

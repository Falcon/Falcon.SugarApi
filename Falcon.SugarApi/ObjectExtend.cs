﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;

namespace Falcon.SugarApi
{
    /// <summary>
    /// Object类型扩展方法
    /// </summary>
    public static class ObjectExtend
    {
        /// <summary>
        /// 对source进行浅表复制，并复制到target中
        /// </summary>
        /// <typeparam name="TSource">原对象类型</typeparam>
        /// <param name="source">原对象</param>
        /// <param name="target">目标对象</param>
        public static TSource CloneTo<TSource>(this TSource source,[NotNull] object target) where TSource : class {
            _=source??throw new ArgumentNullException(nameof(source));
            _=target??throw new ArgumentNullException(nameof(target));
            var all = from s in source.GetType().GetProperties()
                      join t in target.GetType().GetProperties() on s.Name equals t.Name
                      select new { s,t };
            foreach(var item in all) {
                //item.t.SetValue(target, Convert.ChangeType(item.s.GetValue(source), item.t.Type));
                item.t.SetValue(target,item.s.GetValue(source).ChangeType(item.t.PropertyType));
            }
            return source;
        }

        /// <summary>
        /// 从原对象中浅表复制属性值
        /// </summary>
        /// <typeparam name="Ttarget">目标对象类型</typeparam>
        /// <param name="target">目标对象</param>
        /// <param name="source">原对象</param>
        /// <returns>目标对象</returns>
        public static Ttarget CloneFrom<Ttarget>(this Ttarget target,object source) where Ttarget : class {
            source.CloneTo(target);
            return target;
        }

        /// <summary>
        /// 将对象转换成另一类型，如果转换失败可能返回null。
        /// </summary>
        /// <param name="source">原对象</param>
        /// <param name="targetType">目标类型</param>
        /// <returns>转换后的类型</returns>
        public static object? ChangeType(this object? source,Type targetType) {
            if(targetType==null) {
                throw new ArgumentNullException("targetType");
            }
            if(source==null) {
                return null;
            }
            if(targetType.IsGenericType&&targetType.GetGenericTypeDefinition().Equals(typeof(Nullable<>))) {
                NullableConverter nullableConverter = new NullableConverter(targetType);
                targetType=nullableConverter.UnderlyingType;
            }
            return Convert.ChangeType(source,targetType);
        }

        /// <summary>
        /// 如果对象为null则抛出异常
        /// </summary>
        /// <param name="obj">检测对象</param>
        /// <returns>对象本身</returns>
        /// <exception cref="ArgumentNullException">对象为null</exception>
        public static object? ThrowNullExceptionWhenNull(this object? obj) {
            if(obj==null) {
                throw new ArgumentNullException();
            }
            return obj;
        }

        /// <summary>
        /// 扩展对象，获取属性枚举s
        /// </summary>
        /// <param name="obj">要扩展的对象</param>
        /// <returns></returns>
        public static IEnumerable<ExpandPropertyInfo> ExpandProperties(this object obj) {
            foreach(PropertyInfo p in obj.GetType().GetProperties()) {
                yield return new ExpandPropertyInfo {
                    Name=p.Name,
                    Type=p.PropertyType,
                    Value=p.CanRead ? p.GetValue(obj) : null,
                };
            }
        }

        /// <summary>
        /// 对象是否为null
        /// </summary>
        /// <param name="obj">要测试的对象</param>
        /// <returns>True表示对象为null，否则不为null</returns>
        public static bool IsNull([AllowNull] this object obj) => obj==null;

        /// <summary>
        /// 对象是否不为null。与IsNull相反
        /// </summary>
        /// <param name="obj">要测试的对象</param>
        /// <returns>True表示对象不为null，否则为null</returns>
        public static bool IsNotNull([AllowNull] this object obj) => !obj.IsNull();
    }

    /// <summary>
    /// 展开的属性信息
    /// </summary>
    public class ExpandPropertyInfo
    {
        /// <summary>
        /// 属性名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 属性类型
        /// </summary>
        public Type Type { get; set; }
        /// <summary>
        /// 属性值
        /// </summary>
        public object? Value { get; set; }
    }

}

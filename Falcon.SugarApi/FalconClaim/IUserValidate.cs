﻿using System.Security.Claims;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// 认证时候提供二次验证方法
    /// </summary>
    public interface IUserValidate
    {
        /// <summary>
        /// 在认证时候提供二次认证的方法
        /// </summary>
        /// <param name="identity">身份</param>
        void UserLogin(ClaimsIdentity identity);
    }
}

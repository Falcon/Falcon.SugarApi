﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// 通过声明生成token
    /// </summary>
    public interface ITokenBuilter
    {
        /// <summary>
        /// 根据声称组生成token
        /// </summary>
        /// <param name="claims">一组声明</param>
        /// <returns>token</returns>
        string? GetToken(List<Claim> claims);
        /// <summary>
        /// 通过token获取声明组
        /// </summary>
        /// <param name="token">token</param>
        /// <returns>声明组</returns>
        List<Claim>? GetClaims(string token);
    }
}

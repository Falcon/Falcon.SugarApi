﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// 自定义验证方式
    /// </summary>
    public class FalconAuthenticationHandler : IAuthenticationHandler
    {
        /// <summary>
        /// 构造自定义身份验证方式
        /// </summary>
        /// <param name="tokenBuilter">token工具</param>
        public FalconAuthenticationHandler(ITokenBuilter tokenBuilter) {
            this.Scheme = null;
            this.Context = null;
            TokenBuilter = tokenBuilter;
        }

        /// <summary>
        /// 验证计划，模式
        /// </summary>
        public AuthenticationScheme? Scheme { get; set; }

        /// <summary>
        /// HttpContext 上下文
        /// </summary>
        public HttpContext? Context { get; set; }

        /// <inheritdoc />
        public Task<AuthenticateResult> AuthenticateAsync() {
            if (!this.Context.Request.Headers.TryGetValue(FalconClaimOption.FalconAuthenticationKey, out var val)) {
                return UnLoginResultTask;
            }
            var token = val.ToString();
            if (token.IsNullOrEmpty()) {
                return UnLoginResultTask;
            }
            if (FalconClaimOption.TokenPrefix.IsNotNullOrEmpty() && !token.StartsWith(FalconClaimOption.TokenPrefix)) {
                return UnLoginResultTask;
            }
            try {
                var ticket = GetTicket(token);
                if (ticket == null) {
                    return UnLoginResultTask;
                }
                return Task.FromResult(AuthenticateResult.Success(ticket));
            }
            catch (Exception) {
                return UnLoginResultTask;
            }
        }

        /// <inheritdoc />
        public Task ChallengeAsync(AuthenticationProperties? properties) {
            this.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        public Task ForbidAsync(AuthenticationProperties? properties) {
            this.Context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        public Task InitializeAsync(AuthenticationScheme scheme, HttpContext context) {
            this.Scheme = scheme; this.Context = context;
            return Task.CompletedTask;
        }

        /// <summary>
        /// 根据登录token获取票据
        /// </summary>
        private AuthenticationTicket? GetTicket(string token) {
            token.ThrowNullExceptionWhenNull();
            List<Claim>? claims = null;
            try {
                claims = this.TokenBuilter.GetClaims(token);
            }
            catch (Exception) {
                return null;
            }
            if (claims == null || claims.Count == 0) {
                return null;
            }
            //检测是否设置过期时间
            var dtExp = claims.Where(m => m.Type == ClaimTypes.Expiration);
            if (dtExp.Any() && !DateTime.TryParse(dtExp.First().Value, out var et) && et > DateTime.Now) {
                return null;
            }
            var cid = new ClaimsIdentity(FalconClaimOption.SchemeName);
            cid.AddClaims(claims);
            var principal = new ClaimsPrincipal(cid);
            return new AuthenticationTicket(principal, this.Scheme.Name);
        }

        /// <summary>
        /// 验证失败任务
        /// </summary>
        //public static Task<AuthenticateResult> UnLoginResultTask => throw new ApiDefinistions.ApiException("未登录");
        public static Task<AuthenticateResult> UnLoginResultTask => Task.FromResult(AuthenticateResult.Fail("未登录"));

        /// <summary>
        /// Token工具
        /// </summary>
        public ITokenBuilter TokenBuilter { get; }
    }
}

﻿using System.Security.Claims;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// 名字声明
    /// </summary>
    public class NameClaim:Claim
    {
        /// <summary>
        /// 提供名字实现名字声明
        /// </summary>
        /// <param name="name">名字</param>
        public NameClaim(string name) : base(ClaimTypes.Name,name) { }
    }
}

﻿using Microsoft.AspNetCore.Builder;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// 扩展IApplicationBuilder
    /// </summary>
    public static class IApplicationBuilderExtend
    {
        /// <summary>
        /// 使用认证UseAuthentication和授权UseAuthorization。需要加在在UseRouting后UseEndpoints前
        /// </summary>
        public static IApplicationBuilder UseFalconClaim(this IApplicationBuilder app) {
            app.UseAuthentication().UseAuthorization();
            return app;
        }
    }
}

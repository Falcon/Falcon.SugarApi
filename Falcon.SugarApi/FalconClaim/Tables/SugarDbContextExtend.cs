﻿using Falcon.SugarApi.DatabaseDefinitions;

namespace Falcon.SugarApi.FalconClaim.Tables
{
    /// <summary>
    /// 数据库扩展方法
    /// </summary>
    public static class SugarDbContextExtend
    {
        /// <summary>
        /// 初始化FalconClaim角色相关表
        /// </summary>
        /// <param name="dbContext">数据库上下文</param>
        /// <returns>数据库上下文</returns>
        public static SugarDbContext InitFalconClaimRoleDbTables(this SugarDbContext dbContext) {
            dbContext.CodeFirst.InitTables<FalconClaim_Roles>();
            dbContext.CodeFirst.InitTables<FalconClaim_UserInRoles>();
            return dbContext;
        }
    }
}

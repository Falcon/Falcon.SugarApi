﻿namespace Falcon.SugarApi.FalconClaim.Tables
{
    /// <summary>
    /// 用户角色对应关系
    /// </summary>
    public class FalconClaim_UserInRoles
    {
        /// <summary>
        /// 用户主键
        /// </summary>
        public string UserKey { get; set; }
        /// <summary>
        /// 角色名
        /// </summary>
        public string RoleName { get; set; }
    }
}

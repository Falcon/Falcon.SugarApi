﻿namespace Falcon.SugarApi.FalconClaim.Tables
{
    /// <summary>
    /// 角色定义
    /// </summary>
    public class FalconClaim_Roles
    {     
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// 角色说明
        /// </summary>
        public string RoleDescription { get; set; }

    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// SwaggerGenOptions扩展
    /// </summary>
    public static class SwaggerGenOptionsExtend
    {
        /// <summary>
        /// 在swaggerUI中为有FalconClaimAuthorizeAttribute特性的Action增加token头
        /// </summary>
        /// <param name="options">SwaggerGenOptions</param>
        /// <returns>SwaggerGenOptions</returns>
        public static SwaggerGenOptions AddHeaderForFalconClaim(this SwaggerGenOptions options) {
            options.OperationFilter<AddFalconClaimHeaderFilter>();
            return options;
        }

        /// <summary>
        /// 在swaggerUI中为所有api请求增加token头
        /// </summary>
        /// <param name="options">SwaggerGenOptions</param>
        /// <returns>SwaggerGenOptions</returns>
        public static SwaggerGenOptions AddHeaderForAllApi(this SwaggerGenOptions options) {
            options.AddSecurityDefinition(FalconClaimOption.SchemeName,new OpenApiSecurityScheme {
                Description = FalconClaimOption.Description,
                Name = FalconClaimOption.FalconAuthenticationKey,
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey,
                Scheme = FalconClaimOption.SchemeName,
            });
            options.AddSecurityRequirement(new OpenApiSecurityRequirement {
                     {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {
                                Type = ReferenceType.SecurityScheme,
                                Id =FalconClaimOption.SchemeName,
                            }
                        },
                        new List<string>()
                    }
                });
            return options;
        }
    }
}

﻿using System.Collections.Generic;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// FalconClaim验证选项
    /// </summary>
    public class FalconClaimOption
    {
        /// <summary>
        /// 自定义验证方式名
        /// </summary>
        public static string SchemeName { get; set; } = "FalconClaimSchemeName";

        /// <summary>
        /// 用于提供验证token的键
        /// </summary>
        public static string FalconAuthenticationKey { get; set; } = "Falcon_WWW_Authenticate";

        /// <summary>
        /// 用于加密的key
        /// </summary>
        public static string SecKey { get; set; } = "E9319792CB7249AD8E432000E9F2FE7A";

        /// <summary>
        /// 验证头说明
        /// </summary>
        public static string Description { get; set; } = "FalconClaim验证的Token。";

        /// <summary>
        /// 请求Token前缀
        /// </summary>
        public static string TokenPrefix { get; set; } = "";

        /// <summary>
        /// 二次认证方法
        /// </summary>
        public List<IUserValidate> UserValidates { get; set; } = new List<IUserValidate>();
    }
}

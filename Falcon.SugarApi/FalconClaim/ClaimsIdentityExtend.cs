﻿using System.Security.Claims;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// 身份扩展
    /// </summary>
    public static class ClaimsIdentityExtend
    {

        /// <summary>
        /// 象身份中增加声明
        /// </summary>
        /// <param name="cid">身份</param>
        /// <param name="ClaimTypes">声明类型</param>
        /// <param name="value">声明值</param>
        /// <returns>身份</returns>
        public static ClaimsIdentity AddClaim(this ClaimsIdentity cid,string ClaimTypes,string value) {
            cid.AddClaim(new Claim(ClaimTypes,value));
            return cid;
        }
    }
}

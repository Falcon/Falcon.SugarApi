﻿namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// 用户登录凭据模板
    /// <para>用于验证用户登录，可以实现自己的类进行验证</para>
    /// </summary>
    public class UserLoginModel
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string? UserName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string? Password { get; set; }
        /// <summary>
        /// 过期小时数。0或者空为不限制
        /// </summary>
        public int? ExpHours { get; set; }
    }
}

﻿using System;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// 用户登录凭据模板
    /// <para>登录结果，Token作为票据返回。</para>
    /// </summary>
    public class UserLoginResult
    {
        /// <summary>
        /// 登录结果。成功返回True，失败返回False
        /// </summary>
        public bool Success { get; set; } = false;
        /// <summary>
        /// 登录结果信息
        /// </summary>
        public string? Msg { get; set; }
        /// <summary>
        /// 登录票据Token
        /// </summary>
        public string? Token { get; set; }
        /// <summary>
        /// 登录时间。默认当前系统时间
        /// </summary>
        public DateTime? LoginTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 获取一条成功的登录信息
        /// </summary>
        /// <param name="token">成功登录Token</param>
        /// <returns>成功登录结果</returns>
        public static UserLoginResult LoginSuccess(string token)
            => new UserLoginResult { Success = true,Token = token,Msg = "ok" };

        /// <summary>
        /// 登录失败。
        /// </summary>
        /// <param name="msg">失败原因</param>
        /// <returns>失败的登录结果</returns>
        public static UserLoginResult LoginFail(string? msg)
           => new UserLoginResult { Success = false,Msg = msg };
    }
}

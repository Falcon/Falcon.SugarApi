FalconClaim 使用说明:


1. 用户登录：模块提供了UserLoginModel和UserLoginResult类，分别负责提供登录凭据和登录结果，可以扩展使用，当然也可以使用自己的模型类。
> 1.通过UserLoginModel提供的凭据验证用户合法性。   
> 2.通过ITokenBuilter.GetToken方法获取登录token。该方法需要一个Claim列表，该列表至少应包含一个name和role声明，表示登录的用户名和角色，也可以根据需要增加其他声明。  
> 3.构造UserLoginResult对象返回token。
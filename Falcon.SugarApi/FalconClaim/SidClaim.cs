﻿using System.Security.Claims;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// 安全标识声明
    /// </summary>
    public class SidClaim:Claim
    {

        /// <summary>
        /// 提供名字实现名字声明
        /// </summary>
        /// <param name="sid">安全标识</param>
        public SidClaim(string sid) : base(ClaimTypes.Sid,sid) { }

    }
}

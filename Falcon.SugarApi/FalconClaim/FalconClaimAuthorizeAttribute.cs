﻿using Microsoft.AspNetCore.Authorization;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// 根据角色验证
    /// </summary>
    public class FalconClaimAuthorizeAttribute:AuthorizeAttribute
    {
        /// <summary>
        /// 通过提供的角色进行验证
        /// </summary>
        public FalconClaimAuthorizeAttribute() {
            base.AuthenticationSchemes = FalconClaimOption.SchemeName;
        }
        /// <summary>
        /// 通过提供的角色进行验证
        /// </summary>
        /// <param name="roles">,号分割的角色列表</param>
        public FalconClaimAuthorizeAttribute(string roles) {
            base.AuthenticationSchemes = FalconClaimOption.SchemeName;
            base.Roles = roles;
        }

    }

}

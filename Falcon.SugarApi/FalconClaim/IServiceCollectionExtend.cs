﻿using Falcon.SugarApi.Encryption;
using Falcon.SugarApi.JsonSerialize;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;

namespace Falcon.SugarApi.FalconClaim
{
    /// <summary>
    /// 扩展IServiceCollection
    /// </summary>
    public static class IServiceCollectionExtend
    {
        /// <summary>
        /// 增加FalconClaim相关服务。包括ITokenBuilter和FalconAuthenticationHandler
        /// </summary>
        public static IServiceCollection AddFalconClaim(this IServiceCollection services, Action<FalconClaimOption>? optionBuilder = null) {
            var option = new FalconClaimOption();
            optionBuilder?.Invoke(option);
            services.TryAddSingleton<JsonSerializeFactory>();
            services.TryAddSingleton<ITokenBuilter, AesTokenBuilder>();
            services.TryAddSingleton<AESConfig>();
            services.TryAddSingleton<IAESEncryption, AESProvider>();
            services.AddAuthentication(b => {
                b.DefaultAuthenticateScheme = FalconClaimOption.SchemeName;
                b.DefaultChallengeScheme = FalconClaimOption.SchemeName;
                b.DefaultForbidScheme = FalconClaimOption.SchemeName;
                b.AddScheme<FalconAuthenticationHandler>(FalconClaimOption.SchemeName, FalconClaimOption.SchemeName);
            });
            return services;
        }
    }
}

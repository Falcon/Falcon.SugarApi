var myjs={
    service(){
        var opt = {
            method: "post",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: {},
            credentials: 'omit',
            mode: 'cors',
        };
        //HTTP错误处理
        var cs = async function (r) {
            if (r.status >= 200 && r.status < 300) {
                return r;
            }
            if (r.status == 400) {
                let emodel = await r.json();
                if (emodel.Id) {
                    let msg = '<p>错误编号:' + emodel.Id + '</p><p>错误信息' + emodel.Message + '</p><p>发生时间:' + emodel.CreateDateTime + '</p>';
                    console.log(msg);
                    const error = new Error("400");
                    error.response = emodel;
                    throw error;
                }
            }
            let msg = r.status + ":" + r.statusText;
            console.log(msg);
            const error = new Error(r.statusText);
            error.response = r;
            throw error;
        };
        return {
            async data(url,data){
                url = new URL(url,serviceOption.url).href;
                opt.body = typeof data == 'object' ? JSON.stringify(data) : data;
                return fetch(url, opt)
                    .catch(err => {
                        console.log(err);
                        return Promise.reject(err);
                    })
                    .then(r => cs(r)).then(j => {
                        return j.json();
                    });
            },
        }
    }
};
console.log('myjs.js OK!');
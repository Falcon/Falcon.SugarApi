﻿using System;

namespace Falcon.SugarApi.ApiDefinistions
{
    /// <summary>
    /// 一条记录
    /// </summary>
    public class OneRecoder
    {
        /// <summary>
        /// 数据ID
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 操作员
        /// </summary>
        public string OptionBy { get; set; }
    }
}
﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Falcon.SugarApi.ApiDefinistions.Middleware
{
    /// <summary>
    /// ASP.NET CORE 中间件基类，模板类
    /// </summary>
    public abstract class MiddlewareBase
    {
        /// <summary>
        /// 下一步请求
        /// </summary>
        private readonly RequestDelegate Next;

        /// <summary>
        /// 构造中间件
        /// </summary>
        /// <param name="next">下一步</param>
        protected MiddlewareBase(RequestDelegate next) {
            Next = next;
        }

        /// <summary>
        /// 执行中间件
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>任务</returns>
        public virtual async Task InvokeAsync(HttpContext context) {
            await BeforeNext(context);
            await this.Next(context);
            await AfterNext(context);
        }

        /// <summary>
        /// 在下一步之前执行的代码。由InvokeAsync调用
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>任务</returns>
        public virtual async Task BeforeNext(HttpContext context) {
            await Task.CompletedTask;
        }

        /// <summary>
        /// 在下一步之后执行的代码。由InvokeAsync调用
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>任务</returns>
        public virtual async Task AfterNext(HttpContext context) {
            await Task.CompletedTask;
        }
    }
}

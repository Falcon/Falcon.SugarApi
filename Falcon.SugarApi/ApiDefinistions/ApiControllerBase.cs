﻿using Falcon.SugarApi.DatabaseDefinitions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace Falcon.SugarApi.ApiDefinistions
{
    /// <summary>
    /// api控制器基类
    /// </summary>
    [Area("api")]
    [ApiController]
    [Route("[Area]/[Controller]/[Action]")]
    public abstract class ApiControllerBase : ControllerBase
    {
        /// <summary>
        /// 日志记录服务
        /// </summary>
        public ILogger Logger { get; set; }
        /// <summary>
        /// 服务集合
        /// </summary>
        public IServiceProvider Services { get; set; }
        /// <summary>
        /// Sugar数据库
        /// </summary>
        public SugarDbContext SugarDb { get; set; }

        /// <summary>
        /// 应用程序跟目录
        /// </summary>
        public string AppPath { get => AppDomain.CurrentDomain.BaseDirectory; }

        /// <summary>
        /// 获取请求的方法前缀
        /// </summary>
        protected virtual string Prefix {
            get {
                var con = this.RouteData.Values["controller"];
                var ac = this.RouteData.Values["action"];
                return $":{con}:{ac}";
            }
        }
        /// <summary>
        /// 构造控制器基类
        /// </summary>
        /// <param name="service"></param>
        protected ApiControllerBase(IServiceProvider service) {
            this.Services = service;
            this.Logger = service.GetService(typeof(ILogger<>).MakeGenericType(GetType())) as ILogger ?? throw new NullReferenceException("ILogger");
            this.SugarDb = service.GetService<SugarDbContext>() ?? throw new NullReferenceException("SugarDbContext");
        }

        /// <summary>
        /// 保存文本日志
        /// </summary>
        /// <param name="msg">日志文本</param>
        protected virtual void SaveLogger(string msg) => this.Logger.LogInformation(msg);

        /// <summary>
        /// 记录保存请求和响应日志
        /// </summary>
        /// <typeparam name="TRequest">请求信息类型</typeparam>
        /// <typeparam name="TResponse">响应信息类型</typeparam>
        /// <param name="data">请求数据</param>
        /// <param name="result">响应数据</param>
        /// <param name="exception">异常</param>
        protected virtual void SaveLogger<TRequest, TResponse>(TRequest data, TResponse result, Exception? exception = null) {
            var requestStr = this.JsonSerialize(data);
            var responseStr = this.JsonSerialize(result);
            var exStr = exception == null ? "" : exception.ToString();
            var logmsg = $"{this.Prefix}\n请求消息:{requestStr}\n响应消息:{responseStr}\n异常:{exStr}\n";
            this.Logger.LogInformation(logmsg);
        }

        /// <summary>
        /// 从对象序列化字符串
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="obj">要序列化的对象</param>
        /// <returns>字符串</returns>
        protected string JsonSerialize<T>(T obj) {
            return JsonSerializer.Serialize<T>(obj, new JsonSerializerOptions {
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.All),
            });
        }
        /// <summary>
        /// 从字符串反序列化对象
        /// </summary>
        /// <typeparam name="T">对象的类型</typeparam>
        /// <param name="json">json字符串</param>
        /// <returns>对象实例</returns>
        protected T? JsonDeserialize<T>(string json) where T : class {
            return JsonSerializer.Deserialize<T>(json);
        }

        /// <summary>
        /// 抛出api异常
        /// </summary>
        /// <param name="msg">异常消息</param>
        /// <param name="innException">内部异常</param>
        /// <exception cref="ApiException">api异常</exception>
        protected virtual void ThrowApiException(string msg, Exception innException)
            => throw new ApiException(msg, innException);
        /// <summary>
        /// 抛出api异常
        /// </summary>
        /// <param name="msg">异常消息</param>
        /// <exception cref="ApiException">api异常</exception>
        protected virtual void ThrowApiException(string msg)
            => throw new ApiException(msg);
        /// <summary>
        /// 抛出api异常
        /// </summary>
        /// <param name="innException">内部异常</param>
        /// <exception cref="ApiException">api异常</exception>
        protected virtual void ThrowApiException(Exception innException)
            => throw new ApiException(innException.Message, innException);

        /// <summary>
        /// 抛出api异常
        /// </summary>
        /// <param name="msg">异常消息</param>
        /// <param name="innException">内部异常</param>
        /// <param name="exAction">异常处理方法</param>
        protected virtual void ThrowApiException(string msg, Exception innException, Action<ApiException> exAction) {
            var ex = new ApiException(msg, innException);
            exAction?.Invoke(ex);
            throw ex;
        }
    }
}

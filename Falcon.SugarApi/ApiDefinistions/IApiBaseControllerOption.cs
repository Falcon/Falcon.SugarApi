﻿using Falcon.SugarApi.DatabaseDefinitions;
using System.Collections.Generic;

namespace Falcon.SugarApi.ApiDefinistions
{
    /// <summary>
    /// 定义api基础实现方法
    /// </summary>
    /// <typeparam name="T">api对应的数据类型</typeparam>
    public interface IApiBaseControllerOption<T> where T : SugarTableBase
    {
        /// <summary>
        /// 增加数据
        /// </summary>
        /// <param name="data">数据实体</param>
        /// <returns>增加后的数据实体</returns>
        T AddOne(T data);
        /// <summary>
        /// 根据id删除一个数据记录
        /// </summary>
        /// <param name="filter">删除条件，仅提供数据id</param>
        /// <returns>删除后的数据实体</returns>
        T DeleteOne(OneRecoder filter);
        /// <summary>
        /// 修改实体数据
        /// </summary>
        /// <param name="data">数据实体</param>
        /// <returns>修改后的数据实体</returns>
        T Edit(T data);
        /// <summary>
        /// 获取一个测试数据
        /// </summary>
        /// <returns>一个测试记录</returns>
        T GetTest();
        /// <summary>
        /// 分页获取数据
        /// </summary>
        /// <param name="page">分页数据</param>
        /// <returns>数据列表</returns>
        IEnumerable<T> ListAll(PageData page);
        /// <summary>
        /// 根据id获取一条记录
        /// </summary>
        /// <param name="filter">删除条件，仅提供数据id</param>
        /// <returns>删除后的数据列表</returns>
        IEnumerable<T> GetOne(OneRecoder filter);
    }
}
﻿namespace Falcon.SugarApi.ApiDefinistions
{
    /// <summary>
    /// 分页数据
    /// </summary>
    public class PageData
    {
        /// <summary>
        /// 页号码
        /// </summary>
        public int Page { get; set; } = 0;
        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize { get; set; } = 50;
        /// <summary>
        /// 排序依据 列名 desc倒序
        /// </summary>
        public string OrderBy { get; set; } = "ID DESC";
    }
}
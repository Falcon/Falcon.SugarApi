﻿using System;

namespace Falcon.SugarApi.ApiDefinistions
{
    /// <summary>
    /// api异常基类
    /// </summary>
    public class ApiException : Exception
    {
        /// <summary>
        /// 异常信息Id
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// 通过异常信息创建异常
        /// </summary>
        /// <param name="message">异常信息</param>
        public ApiException(string message) : base(message)
        {
        }
        /// <summary>
        /// 通过异常信息和内部异常创建异常
        /// </summary>
        /// <param name="message">异常信息</param>
        /// <param name="innerException">内部异常</param>
        public ApiException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}

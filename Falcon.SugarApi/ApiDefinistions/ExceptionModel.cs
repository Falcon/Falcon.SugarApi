﻿namespace Falcon.SugarApi.ApiDefinistions
{
    /// <summary>
    /// 异常返回模型
    /// </summary>
    public class ExceptionModel
    {
        /// <summary>
        /// 异常编号
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 异常信息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 异常发生时间
        /// </summary>
        public string CreateDateTime { get; set; }
    }
}

﻿using Falcon.SugarApi.DatabaseDefinitions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Falcon.SugarApi.ApiDefinistions
{
    /// <summary>
    /// api接口基础数据操作控制器
    /// </summary>
    /// <typeparam name="T">数据类型</typeparam>
    public abstract class ApiBaseOptionController<T> : ApiControllerBase, IApiBaseControllerOption<T> where T : SugarTableBase, new()
    {
        /// <summary>
        /// 实例化基础数据操作控制器
        /// </summary>
        /// <param name="service">服务提供器</param>
        public ApiBaseOptionController(IServiceProvider service) : base(service) {
        }
        /// <summary>
        /// 插入一条新记录
        /// </summary>
        /// <param name="data">要增加的数据</param>
        /// <returns>增加以后的数据</returns>
        [HttpPost]
        public virtual T AddOne(T data) {
            this.SugarDb.Insert(data, data.CreateBy);
            return data;
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        /// <param name="filter">删除的数据条件，数据Id</param>
        /// <returns>删除后的数据</returns>
        [HttpPost]
        public virtual T DeleteOne(OneRecoder filter) {
            var id = filter.Id;
            this.SugarDb.Delete<T>(id, filter.OptionBy);
            return this.SugarDb.Queryable<T>().First(m => m.Id == id);
        }
        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="data">要修改的数据</param>
        /// <returns>修改后的数据</returns>
        [HttpPost]
        public virtual T Edit(T data) {
            this.SugarDb.Update(data, data.UpdateBy);
            return data;
        }
        /// <summary>
        /// 查询一条数据
        /// </summary>
        /// <param name="filter">数据查询条件，提供数据ID</param>
        /// <returns>查询到的数据集合</returns>
        [HttpPost]
        public virtual IEnumerable<T> GetOne(OneRecoder filter) {
            return this.SugarDb.Queryable<T>().In(filter.Id).ToList();
        }
        /// <summary>
        /// 获取一条测试数据，仅在测试阶段使用
        /// </summary>
        /// <returns>测试数据</returns>
        [HttpPost]
        public virtual T GetTest() {
            return new T().SetTestModel() as T;
        }
        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <param name="page">分页条件</param>
        /// <returns>分页后的数据列表</returns>
        /// <exception cref="ApiException">数据异常</exception>
        [HttpPost]
        public virtual IEnumerable<T> ListAll(PageData page) {
            if (page.OrderBy.IsNullOrEmpty()) {
                throw new ApiException("必须提供OrderBy，分页将按此进行排序。");
            }
            return this.SugarDb.Queryable<T>().OrderBy(page.OrderBy).ToPageList(page.Page, page.PageSize);
        }

        /// <summary>
        /// 相关表初始化
        /// </summary>
        /// <param name="types">需要初始化的表类型</param>
        protected void DataTableInit(params Type[] types) {
            this.SugarDb.UpdateTableStructure(types);
        }
    }
}

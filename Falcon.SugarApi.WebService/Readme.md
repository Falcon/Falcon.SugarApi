﻿## WebService服务器帮助

### 服务端  

1. 首先定义协议及其实现。
2. 使用IServiceCollection.AddWebServices注册服务及其实现。  
2. 使用IApplicationBuilder.UseRouting()应用路由中间件。
3. 调用IApplicationBuilder.UseWebServiceEndpoint方法应用中间件。  

### 客户端  

1. 调用WebServiceClient.CreateWebServiceClient泛型方法创建客户端。泛型为服务端应以的协议，方法返回协议对应的通信实现。  


### 实例

服务端：
~~~c#
    /// <summary>
    /// Web服务协议
    /// </summary>
    [ServiceContract]
    public interface IWebSvc
    {
        [OperationContract]
        public string HellowWorld(string name);
    }

    /// <summary>
    /// Web服务协议实现
    /// </summary>
    public class WebSvc:IWebSvc
    {
        public string HellowWorld(string name) {
            return $"Hellow world {name}!";
        }
    }

    //Program.cs

    using Falcon.SugarApi.WebService;
    using Server;

    var builder = WebApplication.CreateBuilder(args);
    builder.Services.AddWebService<IWebSvc,WebSvc>();

    var app = builder.Build();
    app.UseRouting();
    app.UseWebServiceEndpoint<IWebSvc>("/WebService.asmx");

    app.Run();

~~~   

客户端：   
~~~c#

    using System.ServiceModel;


    /// <summary>
    /// Web服务协议,与服务端匹配
    /// </summary>
    [ServiceContract]
    internal interface IWebSvc
    {
        [OperationContract]
        Task<string> HellowWorldAsync(string name);
    }
    
    //Program.cs

    using Falcon.SugarApi.WebService;

    var url="http://localhost:5000/WebService.asmx";
    var svc = WebServiceClient.CreateWebServiceClient<IWebSvc>(url);
    var result=await svc.HellowWorldAsync("Jeck");

~~~
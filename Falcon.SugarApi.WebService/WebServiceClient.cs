﻿using System.ServiceModel;

namespace Falcon.SugarApi.WebService
{
    /// <summary>
    /// WebService客户端
    /// </summary>
    public static class WebServiceClient
    {
        /// <summary>
        /// 创建一个符合<typeparamref name="T"/>协议的WebService客户端
        /// </summary>
        /// <typeparam name="T">协议</typeparam>
        /// <param name="url">服务端地址</param>
        /// <param name="builder">创建器</param>
        /// <returns>实例化的协议通信客户端</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static T CreateWebServiceClient<T>(string url,Action<WebServiceClientOption>? builder = null) {
            url=url??throw new ArgumentNullException("url must is not null.");
            var bind = new BasicHttpBinding {
                MaxBufferSize=int.MaxValue,
                ReaderQuotas=System.Xml.XmlDictionaryReaderQuotas.Max,
                MaxReceivedMessageSize=int.MaxValue,
                AllowCookies=true,
            };
            var option = new WebServiceClientOption { Address=url,Binding=bind };
            builder?.Invoke(option);
            return (new ChannelFactory<T>(option.Binding,new EndpointAddress(option.Address))).CreateChannel();
        }
    }
}

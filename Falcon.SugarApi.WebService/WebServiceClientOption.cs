﻿using System.ServiceModel.Channels;

namespace Falcon.SugarApi.WebService
{
    /// <summary>
    /// WebService客户端配置
    /// </summary>
    public class WebServiceClientOption
    {
        /// <summary>
        /// 绑定
        /// </summary>
        public Binding? Binding { get; set; }
        /// <summary>
        /// 服务端地址
        /// </summary>
        public string? Address { get; set; }

    }
}

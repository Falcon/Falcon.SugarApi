﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SoapCore;
using System.Xml;

namespace Falcon.SugarApi.WebService
{
    /// <summary>
    /// 扩展服务集合和应用程序，支持WebService
    /// </summary>
    public static class WebServiceExtend
    {
        /// <summary>
        /// 添加基于Soap协议的WebService服务
        /// </summary>
        /// <typeparam name="Tservice">服务类型</typeparam>
        /// <typeparam name="TImplementation">实现类</typeparam>
        /// <param name="services">服务集合</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddWebService<Tservice, TImplementation>(this IServiceCollection services)
             where Tservice : class where TImplementation : class, Tservice {
            services.AddSoapCore();
            services.TryAddSingleton<Tservice,TImplementation>();
            return services;
        }
        /// <summary>
        /// 添加基于Soap协议的WebService服务组
        /// </summary>
        /// <param name="services">服务集合</param>
        /// <param name="descriptors">要添加的WebService服务说明</param>
        /// <returns>服务集合</returns>
        public static IServiceCollection AddWebServices(this IServiceCollection services,params ServiceDescriptor[] descriptors) {
            services.AddSoapCore();
            if(descriptors==null||descriptors.Length==0) {
                return services;
            }
            foreach(var des in descriptors.Where(d => d!=null)) {
                services.TryAddSingleton(des);
            }
            return services;
        }

        /// <summary>
        /// 增加WebService终结点。在此之前必须首先UseRouting()。
        /// </summary>
        /// <typeparam name="TService">终结点服务</typeparam>
        /// <param name="application">应用创建器</param>
        /// <param name="url">终结点url</param>
        /// <returns>终结点服务</returns>
        public static IApplicationBuilder UseWebServiceEndpoint<TService>(this IApplicationBuilder application,string url="/WebSvc.asmx") {
            var xnm = new XmlNamespaceManager(new NameTable());
            xnm.AddNamespace("soap","http://schemas.xmlsoap.org/soap/envelope/");
            xnm.AddNamespace("xsi","http://www.w3.org/2001/XMLSchema-instance");
            xnm.AddNamespace("xsd","http://www.w3.org/2001/XMLSchema");
            application.UseEndpoints(endpoints => {
                endpoints.UseSoapEndpoint<TService>(url,
                    new SoapEncoderOptions() { XmlNamespaceOverrides=xnm },
                    SoapSerializer.DataContractSerializer,
                    omitXmlDeclaration: false,
                    indentXml: false);
            });
            return application;
        }
    }
}
